#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <stdio.h>

#include "core/bus/bus.h"
#include "core/bus/bus_const.h"

static void test_ram(void);

void add_bus_tests(void)
{
    CU_pSuite suite = CU_add_suite("bus test", 0, 0);

    CU_add_test(suite, "ram test", test_ram);
}

static void test_ram(void)
{
    bus *bus = init_bus(NULL, NULL, NULL);
    for (int i = 0; i < RAM_MIRRORS_START; i++)
    {
        CU_ASSERT_EQUAL(read_mem8(i, bus), 0);
        write_mem8(i, i % 0xFF, bus);
        CU_ASSERT_EQUAL(read_mem8(i, bus), i % 0xFF);
        CU_ASSERT_EQUAL(read_mem8(i + RAM_MIRRORS_START, bus), i % 0xFF);
        CU_ASSERT_EQUAL(read_mem8(i + RAM_MIRRORS_START * 2, bus), i % 0xFF);
        CU_ASSERT_EQUAL(read_mem8(i + RAM_MIRRORS_START * 3, bus), i % 0xFF);
    }
    free_bus(bus);
}
