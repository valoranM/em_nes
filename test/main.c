#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

#include "bus/bus.h"
#include "cpu/addr_mode.h"
#include "cpu/execute.h"
#include "cpu/flags.h"
#include "cpu/instruction.h"

int main(void)
{
    CU_initialize_registry();

    add_bus_tests();
    add_addr_mode_tests();
    add_flags_tests();
    // add_instrution_tests();
    // add_cpu_execute_test();

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    free_addr_mode();
    return 0;
}
