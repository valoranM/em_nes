#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <stdint.h>
#include <stdio.h>

#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"
#include "core/cpu/instruction.h"

static void test_adc(void);
static void test_and(void);
static void test_asl(void);
static void test_bit(void);
static void test_branch(void);
static void test_cmpaxy(void);
static void test_dec(void);

void add_instrution_tests(void)
{
    CU_pSuite suite = CU_add_suite("cpu instruction", 0, 0);

    CU_add_test(suite, "adc instruction", test_adc);
    CU_add_test(suite, "and instruction", test_and);
    CU_add_test(suite, "asl instruction", test_asl);
    CU_add_test(suite, "bit instruction", test_bit);
    CU_add_test(suite, "branch instruction", test_branch);
    CU_add_test(suite, "cmp instruction", test_cmpaxy);
    CU_add_test(suite, "dec instruction", test_dec);
}

static void test_adc(void)
{
    // 5 + 0x10 + 0
    // cpu *c = init_cpu(init_bus(NULL, NULL));

    // c->a = 0x05;
    // c->p = 0xC2;
    // c->pc = 0;
    // write_mem8(0x00, 0x10, c->bus);
    // adc(c, Immediate);
    // CU_ASSERT_EQUAL(c->a, 0x15);
    // CU_ASSERT_EQUAL(c->p, 0x0);
    // CU_ASSERT_EQUAL(c->pc, 0x1);
    //
    // // 5 + 0x10 + 1
    // c->a = 0x05;
    // c->p = 0xC3;
    // write_mem8(0x01, 0x10, c->bus);
    // adc(c, Immediate);
    // CU_ASSERT_EQUAL(c->a, 0x16);
    // CU_ASSERT_EQUAL(c->p, 0x0);
    // CU_ASSERT_EQUAL(c->pc, 0x2);
    //
    // // 0x05 + 0xfb
    // c->a = 0x05;
    // c->p = 0x00;
    // write_mem8(0x02, 0xfb, c->bus);
    // adc(c, Immediate);
    // CU_ASSERT_EQUAL(c->a, 0x0);
    // CU_ASSERT_EQUAL(c->p, 0x03);
    // CU_ASSERT_EQUAL(c->pc, 0x3);
    //
    // // 0x05 + 0xfb
    // c->a = 0x01;
    // c->p = 0x00;
    // write_mem8(0x03, 0x80, c->bus);
    // adc(c, Immediate);
    // CU_ASSERT_EQUAL(c->a, 0x81);
    // CU_ASSERT_EQUAL(c->p, 0x80);
    // CU_ASSERT_EQUAL(c->pc, 0x4);

    // free_cpu(c);
}

static void test_and(void)
{
    // 5 & 0
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));
    c->a = 0x05;
    c->p = 0xC0;
    c->pc = 0;
    write_mem8(0x00, 0x0, c->bus);
    and(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x0);
    CU_ASSERT_EQUAL(c->p, 0x42);
    CU_ASSERT_EQUAL(c->pc, 0x1);

    // F0 & 80
    c->a = 0xf0;
    c->p = 0x00;
    write_mem8(0x01, 0x80, c->bus);
    and(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x80);
    CU_ASSERT_EQUAL(c->p, 0x80);
    CU_ASSERT_EQUAL(c->pc, 0x2);

    free_cpu(c);
}

static void test_asl(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));

    // 01 << 1:
    c->a = 0x01;
    c->p = 0x00;
    c->pc = 0;
    asl(c, Accumaltor);
    CU_ASSERT_EQUAL(c->a, 0x02);
    CU_ASSERT_EQUAL(c->p, 0x00);
    CU_ASSERT_EQUAL(c->pc, 0x0);

    // 00 << 1:
    c->a = 0x00;
    c->p = 0x00;
    asl(c, Accumaltor);
    CU_ASSERT_EQUAL(c->a, 0x00);
    CU_ASSERT_EQUAL(c->p, 0x02);
    CU_ASSERT_EQUAL(c->pc, 0x0);

    // 00 << 1:
    c->a = 0x00;
    c->p = 0x00;
    asl(c, Accumaltor);
    CU_ASSERT_EQUAL(c->a, 0x00);
    CU_ASSERT_EQUAL(c->p, 0x02);
    CU_ASSERT_EQUAL(c->pc, 0x0);

    // 40 << 1:
    c->a = 0x40;
    c->p = 0x00;
    asl(c, Accumaltor);
    CU_ASSERT_EQUAL(c->a, 0x80);
    CU_ASSERT_EQUAL(c->p, 0x80);
    CU_ASSERT_EQUAL(c->pc, 0x0);

    // 80 << 1:
    c->a = 0x81;
    c->p = 0x00;
    asl(c, Accumaltor);
    CU_ASSERT_EQUAL(c->a, 0x02);
    CU_ASSERT_EQUAL(c->p, 0x01);
    CU_ASSERT_EQUAL(c->pc, 0x0);

    free_cpu(c);
}

static void test_bit(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));
    c->pc = 0;

    write_mem8(0x00, 0x20, c->bus);
    write_mem8(0x20, 0x80, c->bus);
    c->a = 0x08;
    bit(c, Zero_Page);
    CU_ASSERT_EQUAL(c->p, 0xa6);

    write_mem8(0x01, 0x21, c->bus);
    write_mem8(0x21, 0xc0, c->bus);
    c->a = 0x80;
    bit(c, Zero_Page);
    CU_ASSERT_EQUAL(c->p, 0xe4);

    free_cpu(c);
}

static void test_branch(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));
    c->pc = 0;
    write_mem8(0x60, 0x20, c->bus);

    // BPL
    c->pc = 0x60;
    c->p = 0x80;
    branch(c, BPL);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BPL);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BMI
    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BMI);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x80;
    branch(c, BMI);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BVC
    c->pc = 0x60;
    c->p = 0x40;
    branch(c, BVC);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BVC);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BVS
    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BVS);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x40;
    branch(c, BVS);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BCC
    c->pc = 0x60;
    c->p = 0x01;
    branch(c, BCC);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BCC);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BVS
    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BCS);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x01;
    branch(c, BCS);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BNE
    c->pc = 0x60;
    c->p = 0x02;
    branch(c, BNE);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BNE);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    // BEQ
    c->pc = 0x60;
    c->p = 0x00;
    branch(c, BEQ);
    CU_ASSERT_EQUAL(c->pc, 0x61);

    c->pc = 0x60;
    c->p = 0x02;
    branch(c, BEQ);
    CU_ASSERT_EQUAL(c->pc, 0x81);

    free_cpu(c);
}

static void test_cmpaxy(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));
    c->pc = 0;

    c->a = 0x05;
    c->y = 0x05;
    c->x = 0x05;
    c->p = 0x00;
    write_mem8(0x00, 0x5, c->bus);
    cmp(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x03);
    c->p = 0x00;
    c->pc = 0x00;
    cpx(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x03);
    c->p = 0x00;
    c->pc = 0x00;
    cpy(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x03);

    c->pc = 0x00;
    c->p = 0x0;
    write_mem8(0x00, 0x6, c->bus);
    cmp(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x80);
    c->p = 0x00;
    c->pc = 0x00;
    cpx(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x80);
    c->p = 0x00;
    c->pc = 0x00;
    cpy(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x80);

    c->pc = 0x00;
    c->p = 0x0;
    write_mem8(0x00, 0x4, c->bus);
    cmp(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x01);
    c->p = 0x00;
    c->pc = 0x00;
    cpx(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x01);
    c->p = 0x00;
    c->pc = 0x00;
    cpy(c, Immediate);
    CU_ASSERT_EQUAL(c->a, 0x05);
    CU_ASSERT_EQUAL(c->pc, 0x01);
    CU_ASSERT_EQUAL(c->p, 0x01);

    free_cpu(c);
}

static void test_dec(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));

    c->p = 0;
    c->pc = 0;
    write_mem8(0x10, 0x2, c->bus);
    write_mem8(0x0, 0x10, c->bus);
    dec(c, Zero_Page);
    CU_ASSERT_EQUAL(read_mem8(0x10, c->bus), 0x01);
    CU_ASSERT_EQUAL(c->p, 0x00);
    c->pc = 0;
    dec(c, Zero_Page);
    CU_ASSERT_EQUAL(read_mem8(0x10, c->bus), 0x00);
    CU_ASSERT_EQUAL(c->p, 0x02);
    c->pc = 0;
    dec(c, Zero_Page);
    CU_ASSERT_EQUAL(read_mem8(0x10, c->bus), 0xff);
    CU_ASSERT_EQUAL(c->p, 0x80);
    free_cpu(c);
}
