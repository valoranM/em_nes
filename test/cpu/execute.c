#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/TestDB.h>
#include <stdint.h>

#include "addr_mode.h"
#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"

static void test_execute_adc(void);

void add_cpu_execute_test(void)
{
    CU_pSuite suite = CU_add_suite("execute cpu test", 0, 0);

    CU_add_test(suite, "execute adc cpu", test_execute_adc);
}

static void test_execute_adc(void)
{
    cpu *c = init_cpu(init_bus(NULL, NULL, NULL));
    c->pc = 0;

    uint8_t code[] = {0x69, 0x10,       //
                      0x65, 0x60,       //
                      0x75, 0x55,       //
                      0x6D, 0x34, 0x12, //
                      0x7D, 0x30, 0x12, //
                      0x79, 0x30, 0x12, //
                      0x61, 0x2C,       //
                      0x71, 0x30};

    for (uint16_t i = 0; i < sizeof(code); i++)
    {
        write_mem8(i, code[i], c->bus);
    }
    write_mem8(0x60, 0x10, c->bus);
    write_mem16(0x1234, 0x20, c->bus);
    write_mem16(0x1235, 0x30, c->bus);
    write_mem16(0x1236, 0x05, c->bus);
    write_mem16(0x30, 0x1234, c->bus);
    write_mem16(0x1238, 0x1, c->bus);

    c->a = 0x10;

    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x02);
    CU_ASSERT_EQUAL(c->a, 0x20);

    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x04);
    CU_ASSERT_EQUAL(c->a, 0x30);

    c->x = 0x0B;
    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x06);
    CU_ASSERT_EQUAL(c->a, 0x40);

    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x09);
    CU_ASSERT_EQUAL(c->a, 0x60);

    c->x = 0x05;
    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x0C);
    CU_ASSERT_EQUAL(c->a, 0x90);

    c->y = 0x06;
    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x0F);
    CU_ASSERT_EQUAL(c->a, 0x95);

    c->x = 0x04;
    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x11);
    CU_ASSERT_EQUAL(c->a, 0xB5);

    c->y = 0x04;
    execute(c);
    CU_ASSERT_EQUAL(c->pc, 0x13);
    CU_ASSERT_EQUAL(c->a, 0xB6);
}
