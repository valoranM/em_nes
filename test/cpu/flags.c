#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <stdint.h>

#include "core/cpu/flags.h"

static void test_get(void);
static void test_set(void);
static void test_clear(void);

void add_flags_tests(void)
{
    CU_pSuite suite = CU_add_suite("flags cpu", 0, 0);

    CU_add_test(suite, "flags get", test_get);
    CU_add_test(suite, "flags set", test_set);
    CU_add_test(suite, "flags clear", test_clear);
}

static void test_get(void)
{
    int rets[] = {1, 0, 0, 0, 0, 0, 0, 0};
    uint8_t flags = 0x1;
    for (int i = 0; i < 8; i++)
    {
        CU_ASSERT_EQUAL(get_flag(flags, CARRY_F), rets[i % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, ZERO_F), rets[(i + 7) % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, INTERRUPT_F), rets[(i + 6) % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, DECIMAL_F), rets[(i + 5) % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, BREAK_F), rets[(i + 4) % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, OVERFLOW_F), rets[(i + 2) % 8]);
        CU_ASSERT_EQUAL(get_flag(flags, NEGATIVE_F), rets[(i + 1) % 8]);
        flags += 0x1 << i; // added next flags
    }
}

static void test_set(void)
{
    uint8_t flags = 0x0;
    flags = set_flag(flags, CARRY_F);
    CU_ASSERT_EQUAL(flags, 0x01);
    flags = set_flag(flags, ZERO_F);
    CU_ASSERT_EQUAL(flags, 0x03);
    flags = set_flag(flags, INTERRUPT_F);
    CU_ASSERT_EQUAL(flags, 0x07);
    flags = set_flag(flags, DECIMAL_F);
    CU_ASSERT_EQUAL(flags, 0x0F);
    flags = set_flag(flags, BREAK_F);
    CU_ASSERT_EQUAL(flags, 0x1F);
    flags = set_flag(flags, OVERFLOW_F);
    CU_ASSERT_EQUAL(flags, 0x5F);
    flags = set_flag(flags, NEGATIVE_F);
    CU_ASSERT_EQUAL(flags, 0xDF);
}

static void test_clear(void)
{

    uint8_t flags = 0xDF;
    flags = clear_flag(flags, CARRY_F);
    CU_ASSERT_EQUAL(flags, 0xDE);
    flags = clear_flag(flags, ZERO_F);
    CU_ASSERT_EQUAL(flags, 0xDC);
    flags = clear_flag(flags, INTERRUPT_F);
    CU_ASSERT_EQUAL(flags, 0xD8);
    flags = clear_flag(flags, DECIMAL_F);
    CU_ASSERT_EQUAL(flags, 0xD0);
    flags = clear_flag(flags, BREAK_F);
    CU_ASSERT_EQUAL(flags, 0xC0);
    flags = clear_flag(flags, OVERFLOW_F);
    CU_ASSERT_EQUAL(flags, 0x80);
    flags = clear_flag(flags, NEGATIVE_F);
    CU_ASSERT_EQUAL(flags, 0x00);
}
