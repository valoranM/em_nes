#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/TestDB.h>
#include <stdint.h>

#include "addr_mode.h"
#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"

static void test_absolute_addr(void);
static void test_zero_page(void);
static void test_indirect_absolute_addr(void);
static void test_absolute_indexed_x_addr(void);
static void test_absolute_indexed_y_addr(void);
static void test_zerop_indexed_x_addr(void);
static void test_zerop_indexed_y_addr(void);
static void test_indexed_indirect(void);
static void test_indirect_indexed(void);

static cpu *c;

void add_addr_mode_tests(void)
{
    c = init_cpu(init_bus(NULL, NULL, NULL));
    c->pc = 0;

    CU_pSuite suite = CU_add_suite("addresse mode", 0, 0);

    CU_add_test(suite, "absolute mode", test_absolute_addr);
    CU_add_test(suite, "zero page", test_zero_page);
    CU_add_test(suite, "indirect abosulte addr", test_indirect_absolute_addr);
    CU_add_test(suite, "absolute indexed x addr", test_absolute_indexed_x_addr);
    CU_add_test(suite, "absolute indexed y addr", test_absolute_indexed_y_addr);
    CU_add_test(suite, "zerop indexed x addr", test_zerop_indexed_x_addr);
    CU_add_test(suite, "zerop indexed y addr", test_zerop_indexed_y_addr);
    CU_add_test(suite, "indexed indirect addr", test_indexed_indirect);
    CU_add_test(suite, "indirect indexed addr", test_indirect_indexed);
}

void free_addr_mode(void)
{
    free_cpu(c);
}

static void test_absolute_addr(void)
{
    write_mem16(0x10, 0x300, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Absolute, c), 0x300);
    CU_ASSERT_EQUAL(c->pc, 2);
    write_mem16(0x10, 0x100, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Absolute, c), 0x100);
    CU_ASSERT_EQUAL(c->pc, 4);
}

static void test_zero_page(void)
{
    write_mem8(0x10, 0x30, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Zero_Page, c), 0x30);
    CU_ASSERT_EQUAL(c->pc, 5);
    write_mem8(0x11, 0x31, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x11, Zero_Page, c), 0x31);
    CU_ASSERT_EQUAL(c->pc, 6);
}

static void test_indirect_absolute_addr(void)
{
    write_mem16(0x100, 0x1234, c->bus);
    write_mem16(0x10, 0x100, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Indirect_Abs, c), 0x1234);
    CU_ASSERT_EQUAL(c->pc, 8);
}

static void test_absolute_indexed_x_addr(void)
{
    c->x = 2;
    write_mem16(0x10, 0x100, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Ab_Indexed_X, c), 0x102);
    CU_ASSERT_EQUAL(c->pc, 10);
}

static void test_absolute_indexed_y_addr(void)
{
    c->y = 0x2;
    write_mem16(0x10, 0x100, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x10, Ab_Indexed_Y, c), 0x102);
    CU_ASSERT_EQUAL(c->pc, 12);
}

static void test_zerop_indexed_x_addr(void)
{
    c->x = 0x3;
    write_mem8(0x01, 0x80, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x01, Zero_Page_X, c), 0x83);
    CU_ASSERT_EQUAL(c->pc, 13);
    write_mem8(0x02, 0xff, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x02, Zero_Page_X, c), 0x02);
    CU_ASSERT_EQUAL(c->pc, 14);
}

static void test_zerop_indexed_y_addr(void)
{
    c->y = 0x3;
    write_mem8(0x01, 0x40, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x01, Zero_Page_Y, c), 0x43);
    CU_ASSERT_EQUAL(c->pc, 15);
    write_mem8(0x02, 0xff, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x02, Zero_Page_Y, c), 0x02);
    CU_ASSERT_EQUAL(c->pc, 16);
}

static void test_indexed_indirect(void)
{
    c->x = 0x10;
    write_mem16(0x30, 0x1234, c->bus);
    write_mem16(0x00, 0x20, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x00, Indexed_Indirect, c), 0x1234);
    CU_ASSERT_EQUAL(c->pc, 17);
    write_mem16(0xf0, 0x102, c->bus);
    write_mem16(0x04, 0xe0, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x04, Indexed_Indirect, c), 0x0102);
    CU_ASSERT_EQUAL(c->pc, 18);
}

static void test_indirect_indexed(void)
{
    c->y = 0x10;
    write_mem16(0x20, 0x123, c->bus);
    write_mem16(0x00, 0x20, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x00, Indirect_Indexed, c), 0x0133);
    CU_ASSERT_EQUAL(c->pc, 19);
    c->y = 0x20;
    write_mem16(0x10, 0x0103, c->bus);
    write_mem16(0x60, 0x10, c->bus);
    CU_ASSERT_EQUAL(get_addr(0x60, Indirect_Indexed, c), 0x0123);
    CU_ASSERT_EQUAL(c->pc, 20);
}
