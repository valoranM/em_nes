#ifndef EVENT_H
#define EVENT_H

#include <stdbool.h>

#include "core/controllers/joypad.h"

extern bool poll_event(joypad *j);

#endif // !EVENT_H
