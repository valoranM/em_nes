#include <SDL2/SDL.h>
#include <stdbool.h>

#include "core/controllers/joypad.h"

static void key_event(SDL_Event const event, joypad *j, bool pressed);

bool poll_event(joypad *j)
{
    SDL_Event event;
    bool quit = false;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            quit = true;
            break;
        case SDL_KEYDOWN:
            key_event(event, j, true);
            break;
        case SDL_KEYUP:
            key_event(event, j, false);
            break;
        }
    }

    return quit;
}

static void key_event(SDL_Event const event, joypad *j, bool pressed)
{
    switch (event.key.keysym.sym)
    {
    case SDLK_UP:
        set_button_pressed_status(j, B_UP, pressed);
        break;
    case SDLK_DOWN:
        set_button_pressed_status(j, B_DOWN, pressed);
        break;
    case SDLK_RIGHT:
        set_button_pressed_status(j, B_RIGHT, pressed);
        break;
    case SDLK_LEFT:
        set_button_pressed_status(j, B_LEFT, pressed);
        break;
    case SDLK_a:
        set_button_pressed_status(j, B_A, pressed);
        break;
    case SDLK_b:
        set_button_pressed_status(j, B_B, pressed);
        break;
    case SDLK_ESCAPE:
        set_button_pressed_status(j, B_SELECT, pressed);
        break;
    case SDLK_RETURN:
        set_button_pressed_status(j, B_START, pressed);
        break;
    }
}
