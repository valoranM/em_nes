#include <stdio.h>
#include <stdlib.h>

#include "read_card.h"
#include "core/nes.h"
#include "views/utils.h"

static void print_usage(void);

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        print_usage();
        return 0;
    }
    rom *rom = init_rom();
    read_rom(rom, argv[1]);
    nes *nes = init_nes(rom);
    init_sdl();

    run(nes->cpu);

    quit_sdl();
    free_nes(nes);
    return 0;
}

static void print_usage(void)
{
    printf("./nes_emu [nes rom]\n");
}
