#ifndef DRAW_H
#define DRAW_H

#include "core/frame/frame.h"

extern void init_renderer(void);
extern void free_renderer(void);
extern void draw_frame(frame *f);

#endif // !DRAW_H
