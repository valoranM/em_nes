#ifndef UTILS_H
#define UTILS_H

#include <SDL2/SDL.h>

extern SDL_Window *win;

extern void init_sdl(void);
extern void quit_sdl(void);
#endif // !INIT_H
