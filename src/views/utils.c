#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

#include "views/draw.h"
#include "views/utils.h"

SDL_Window *win;

void init_sdl(void)
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
    {
        fprintf(stderr, "SDL init error : %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    win = SDL_CreateWindow("Tile viewer", SDL_WINDOWPOS_CENTERED,
                           SDL_WINDOWPOS_CENTERED, 256 * 3, 240 * 3,
                           SDL_WINDOW_SHOWN);
    if (NULL == win)
    {
        fprintf(stderr, "SDL_CreateWindow error : %s", SDL_GetError());
        quit_sdl();
        exit(EXIT_FAILURE);
    }

    init_renderer();
}

void quit_sdl(void)
{

    if (NULL != win)
    {
        SDL_DestroyWindow(win);
    }
    free_renderer();
    SDL_Quit();
}
