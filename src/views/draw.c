#include <SDL2/SDL.h>

#include "core/frame/frame.h"
#include "views/utils.h"

static SDL_Renderer *renderer;
static void draw_pixel(frame *f, int x, int y);

void init_renderer(void)
{
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    if (NULL == renderer)
    {
        fprintf(stderr, "Erreur SDL_CreateRenderer : %s", SDL_GetError());
        goto exit;
    }

    if (0 != SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255))
    {
        fprintf(stderr, "Erreur SDL_SetRenderDrawColor : %s", SDL_GetError());
        goto exit;
    }

    if (0 != SDL_RenderClear(renderer))
    {
        fprintf(stderr, "Erreur SDL_SetRenderDrawColor : %s", SDL_GetError());
        goto exit;
    }
    SDL_RenderPresent(renderer);
    return;

exit:
    quit_sdl();
    exit(EXIT_FAILURE);
}

void free_renderer(void)
{
    if (NULL != renderer)
    {
        SDL_DestroyRenderer(renderer);
    }
}

void draw_frame(frame *f)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    for (int y = 0; y < HEIGHT; y++)
    {
        for (int x = 0; x < WIDTH; x++)
        {
            draw_pixel(f, x, y);
        }
    }
    SDL_RenderPresent(renderer);
}

static void draw_pixel(frame *f, int x, int y)
{
    SDL_Rect rect = {x * 3, y * 3, 3, 3};
    uint8_t *pixel = get_pixel(f, x, y);
    SDL_SetRenderDrawColor(renderer, pixel[0], pixel[1], pixel[2], 255);
    SDL_RenderFillRect(renderer, &rect);
}
