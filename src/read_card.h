#ifndef READ_H
#define READ_H

#include "core/card/rom.h"

extern void read_rom(rom *rom, const char *file);

#endif // READ_H
