#ifndef ROM_H
#define ROM_H

#include <stdbool.h>
#include <stdint.h>

#define PRG_ROM_BANK_SIZE 0x4000
#define CHR_ROM_BANK_SIZE 0x2000

#define INES_HEADER_SIZE 0x10

enum Mirroring
{
    HORIZONTAL,
    VERTICAL,
    FOUR_SCREEN,
};

enum Version
{
    INES_V1 = 0,
    INES_V2 = 2,
};

struct s_rom
{
    uint8_t *PRG_rom;
    uint8_t PRG_banks;
    uint8_t *CHR_rom;
    uint8_t CHR_banks;
    enum Mirroring mirror;
    enum Version version;
    uint8_t mapper;
    bool trainner;
    bool battery_ram;
};

typedef struct s_rom rom;

extern rom *init_rom(void);

extern uint8_t read_prg_rom(uint16_t addr, rom *rom);

extern void write_prg_rom(uint16_t addr);

extern void free_rom(rom *rom);

#endif // ROM_H
