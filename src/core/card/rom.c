#include <stdint.h>
#include <stdlib.h>

#include "core/bus/bus_const.h"
#include "core/card/rom.h"
#include "utils/debug.h"

rom *init_rom(void)
{
    return malloc(sizeof(rom));
}

uint8_t read_prg_rom(uint16_t addr, rom *rom)
{
    addr -= PRG_START;
    if (rom->PRG_banks < 2)
    {
        addr = addr % PRG_ROM_BANK_SIZE;
    }
    return rom->PRG_rom[addr];
}

void write_prg_rom(uint16_t addr)
{
    error_args("Attempt to write to Cartridge ROM space 'Ox%x'", addr);
}

void free_rom(rom *rom)
{
    if (rom->CHR_rom != NULL)
    {
        free(rom->CHR_rom);
    }
    if (rom->PRG_rom != NULL)
    {
        free(rom->PRG_rom);
    }

    free(rom);
}
