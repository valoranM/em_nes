#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define PPU

#include "core/ppu/ppu.h"
#include "core/ppu/register.h"
#include "core/ppu/vram.h"
#include "utils/debug.h"

uint16_t bknd_pattern_addr(ppu *ppu)
{
    if ((ppu->control & 0x10) != 0)
    {
        return 0x1000;
    }
    return 0x0;
}

uint16_t sprt_pattern_addr(ppu *ppu)
{
    if ((ppu->control & 0x08) != 0)
    {
        return 0x1000;
    }
    return 0x0;
}

// I/O Registers

void write_to_ppu_ctrl(ppu *ppu, uint8_t data)
{
    bool pred_nmi_status = generate_vblank_nmi(ppu->control);
    ppu->control = data;
    if (pred_nmi_status && generate_vblank_nmi(data) &&
        is_in_vblank(ppu->status))
    {
        ppu->NMI_interupt = true;
    }
}

void write_to_ppu_mask(ppu *ppu, uint8_t data)
{
    ppu->mask = data;
}

void write_to_oam_addr(ppu *ppu, uint8_t addr)
{
    ppu->oam_addr = addr;
}

void write_to_oam_data(ppu *ppu, uint8_t data)
{
    ppu->oam_data[ppu->oam_addr] = data;
    ppu->oam_addr++;
}

void write_to_ppu_scroll(ppu *ppu, uint8_t data)
{
    update_scroll_reg(&ppu->scroll, data);
}

void write_to_ppu_addr(ppu *ppu, uint8_t addr)
{
    update_address_reg(&ppu->addr, addr);
}

void write_to_ppu_data(ppu *ppu, uint8_t data)
{
    write_vram(ppu, ppu->addr.address, data);
    increment_address_reg(&ppu->addr, increment_val(ppu->control));
}

void write_to_oam_dma(ppu *ppu, uint8_t data[256])
{
    for (int i = 0; i < 256; i++)
    {
        ppu->oam_data[ppu->oam_addr] = data[i];
        ppu->oam_addr++;
    }
}

// I/O read

uint8_t read_ppu_status(ppu *ppu)
{
    uint8_t status = ppu->status;
    reset_vblank_status(&ppu->status);
    reset_scroll_reg_latch(&ppu->scroll);
    reset_address_reg_latch(&ppu->addr);
    return status;
}

uint8_t read_oam_data(ppu *ppu)
{
    return ppu->oam_data[ppu->oam_addr];
}

static void increment_vrm_addr(ppu *ppu);

uint8_t read_ppu_data(ppu *ppu)
{
    uint16_t addr = ppu->addr.address;
    increment_vrm_addr(ppu);

    uint8_t res = ppu->data_buf;
    ppu->data_buf = read_vram(ppu, addr);

    return res;
}

static void increment_vrm_addr(ppu *ppu)
{
    increment_address_reg(&ppu->addr, increment_val(ppu->control));
}
