#include <stdint.h>
#include <stdio.h>

#include "core/ppu/ppu.h"
#include "core/ppu/vram.h"
#include "utils/debug.h"

static uint16_t mirror_vram_addr(ppu *ppu, uint16_t addr);
static uint16_t palettes_mirroring(uint16_t addr);

uint8_t read_vram(ppu *ppu, uint16_t addr)
{
    if (addr < CHR_ROM_LIMIT)
    {
        return ppu->chr_rom[addr];
    }
    if (addr < NAME_TABLES_LIMIT)
    {
        return ppu->vram[mirror_vram_addr(ppu, addr)];
    }
    if (addr < PALETTE_LIMIT)
    {
        addr = palettes_mirroring(addr);
        return ppu->palettes[(addr - NAME_TABLES_LIMIT) % 0x20];
    }

    warning_args("Ignoring ppu read-access at Ox%X\n", addr);
    return 0;
}

void write_vram(ppu *ppu, uint16_t addr, uint8_t val)
{
    if (addr < CHR_ROM_LIMIT)
    {
        warning_args("Ignoring CHR ppu write-access at Ox%X\n", addr);
    }
    else if (addr < NAME_TABLES_LIMIT)
    {
        ppu->vram[mirror_vram_addr(ppu, addr)] = val;
    }
    else if (addr < PALETTE_LIMIT)
    {
        addr = palettes_mirroring(addr);
        ppu->palettes[(addr - NAME_TABLES_LIMIT) % 0x20] = val;
    }
    else
    {
        warning_args("Ignoring ppu write-access at Ox%X\n", addr);
    }
}

static uint16_t horizontal_addr(uint16_t addr, uint8_t name_table);
static uint16_t vertical_addr(uint16_t addr, uint8_t name_table);

static uint16_t mirror_vram_addr(ppu *ppu, uint16_t addr)
{
    uint16_t mirror_addr = addr & 0x2FFF;
    uint16_t vram_index = mirror_addr - 0x2000;
    uint16_t name_table = vram_index / 0x400;
    if (ppu->mirroring == HORIZONTAL)
    {
        return horizontal_addr(vram_index, name_table);
    }
    if (ppu->mirroring == VERTICAL)
    {
        return vertical_addr(vram_index, name_table);
    }

    return vram_index;
}

/*
    [ A ] [ a ]     [ B ]
    [ B ] [ b ]     [ A ]
 */
static uint16_t horizontal_addr(uint16_t addr, uint8_t name_table)
{
    switch (name_table)
    {
    case 2:
    case 1:
        return addr - 0x400;
    case 3:
        return addr - 0x800;
    default:
        return addr;
    }
}

/*
    [ A ] [ B ]
    [ a ] [ b ]
*/
static uint16_t vertical_addr(uint16_t addr, uint8_t name_table)
{

    switch (name_table)
    {
    case 2:
    case 3:
        return addr - 0x800;
    default:
        return addr;
    }
}

/*  Addr    Mirrors
    0x10 => 0x00
    0x14 => 0x04
    0x18 => 0x08
    0x1C => 0x0C
*/
static uint16_t palettes_mirroring(uint16_t addr)
{
    if (addr == 0x10 || addr == 0x14 || addr == 0x18 || addr == 0x1C)
    {
        return addr - 0x10;
    }
    return addr;
}
