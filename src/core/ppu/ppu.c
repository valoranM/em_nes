#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#define PPU

#include "core/card/rom.h"
#include "core/ppu/ppu.h"
#include "core/ppu/register.h"
#include "core/ppu/vram.h"
#include "utils/debug.h"

static bool is_sprite_0_hit(ppu *ppu, uint8_t cycle);

ppu *init_ppu(rom *rom, frame *frame)
{
    ppu *res = malloc(sizeof(ppu));
    memset(res, 0, sizeof(ppu));
    res->chr_rom = rom->CHR_rom;
    res->addr.hi_addr = true;
    res->scroll.latch = false;
    res->mirroring = rom->mirror;
    res->frame = frame;
    return res;
}

void free_ppu(ppu *ppu)
{
    free(ppu);
}

bool ppu_tick(ppu *ppu, uint8_t cycle)
{
    ppu->cycle = ppu->cycle + cycle;
    if (ppu->cycle >= 341)
    {
        if (is_sprite_0_hit(ppu, cycle))
        {
            set_sprite_zero_hit(&ppu->status);
        }
        ppu->cycle = ppu->cycle - 341;
        ppu->scanline += 1;

        if (ppu->scanline == 241)
        {
            set_vblank_status(&ppu->status);
            reset_sprite_zero_hit(&ppu->status);
            if (generate_vblank_nmi(ppu->control))
            {
                ppu->NMI_interupt = true;
            }
        }

        if (ppu->scanline >= 262)
        {
            ppu->scanline = 0;
            ppu->NMI_interupt = false;
            reset_vblank_status(&ppu->status);
            reset_sprite_zero_hit(&ppu->status);
            return true;
        }
    }
    return false;
}

static bool is_sprite_0_hit(ppu *ppu, uint8_t cycle)
{
    uint8_t x = ppu->oam_data[0];
    uint8_t y = ppu->oam_data[3];
    return (y == ppu->scanline) && x < cycle && show_sprites(ppu->mask);
}
