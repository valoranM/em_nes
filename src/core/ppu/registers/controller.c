#include <stdbool.h>
#include <stdint.h>

#include "core/ppu/register.h"

/*

    +-+-+-+-+-+-+-+-+
    |V|P|H|B|S|I|N|N|
    +-+-+-+-+-+-+-+-+
     | | | | | | | |
     | | | | | | +-+- Base nametable address
     | | | | | |      (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)
     | | | | | +----- VRAM address increment per CPU read/write of PPUDATA
     | | | | |        (0: add 1, going across; 1: add 32, going down)
     | | | | +------- Sprite pattern table address for 8x8 sprites
     | | | |          (0: $0000; 1: $1000; ignored in 8x16 mode)
     | | | +--------- Background pattern table address (0: $0000; 1: $1000)
     | | +----------- Sprite size
     | |              (0: 8x8 pixels; 1: 8x16 pixels – see PPU OAM#Byte 1)
     | +------------- PPU master/slave select
     |                (0: read backdrop from EXT pins;
     |                 1: output color on EXT pins)
     +--------------- Generate an NMI at the start of the
                      vertical blanking interval (0: off; 1: on)
*/

/* xxxxxxNN
 *       00 => 0x2000
 *       01 => 0x2400
 *       10 => 0x2800
 *       11 => 0x2C00
 */
uint8_t nametable_addr(uint8_t reg)
{
    return 0x2000 + (0x400 * (reg & 0x03));
}

/* xxxxxIxx
 *      0   => 0x01
 *      1   => 0x20
 */
uint16_t increment_val(uint8_t reg)
{
    return ((reg & 0x04) == 0) ? 0x01 : 0x20;
}

/* xxxxSxxx
 *     0    => 0x0000
 *     1    => 0x1000
 */
uint16_t sprt_patterntable_addr(uint8_t reg)
{
    return ((reg & 0x08) == 0) ? 0x0 : 0x1000;
}

/* xxxBxxxx
 *    0     => 0x0000
 *    1     => 0x1000
 */
uint16_t bknd_pattentable_addr(uint8_t reg)
{
    return ((reg & 0x10) == 0) ? 0x0 : 0x1000;
}

/* xxHxxxx
 *   0     => 0x08
 *   1     => 0x10
 */
uint8_t sprite_size(uint8_t reg)
{
    return ((reg & 0x20) == 0) ? 0x08 : 0x10;
}

/* xPxxxxx
 *  0      => 0x08
 *  1      => 0x10
 */
uint8_t master_slave_select(uint8_t reg)
{
    return ((reg & 0x40) == 0) ? 0x0 : 0x1;
}

/*
 * Vxxxxxxx
 * 0        => false
 * 1        => true
 *
 */
bool generate_vblank_nmi(uint8_t reg)
{
    return (reg & 0x80) != 0;
}
