#include <stdbool.h>
#include <stdint.h>

#include "core/ppu/register.h"

/*
    scroll_x : uint8
    scroll_y : uint8
    latch    : bool
*/

void update_scroll_reg(scroll_reg *scroll_reg, uint8_t data)
{
    if (!scroll_reg->latch)
    {
        scroll_reg->x_pos = data;
        scroll_reg->latch = true;
    }
    else
    {
        scroll_reg->y_pos = data;
        scroll_reg->latch = false;
    }
}

void reset_scroll_reg_latch(scroll_reg *scroll_reg)
{
    scroll_reg->latch = false;
}
