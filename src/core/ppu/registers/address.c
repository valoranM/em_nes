#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "core/ppu/register.h"

/*
    address : uint16_t
    buff    : uint16_t
    hi_addr : bool
*/

void update_address_reg(addr_reg *addr_reg, uint16_t address)
{
    if (addr_reg->hi_addr)
    {
        // Clean address
        addr_reg->tmp &= 0xFF;
        // address & 0x3F for mirrored address (0x3FFF max)
        addr_reg->tmp = ((address & 0x3F) << 8);
        addr_reg->hi_addr = false;
    }
    else
    {
        addr_reg->tmp &= 0xFF00;
        addr_reg->tmp |= address;
        addr_reg->address = addr_reg->tmp;
        addr_reg->hi_addr = true;
    }
}

void increment_address_reg(addr_reg *addr_reg, uint8_t incr)
{
    uint16_t v = addr_reg->address + incr;
    if (v > 0x3FFF)
    {
        v &= 0x3FFF;
    }
    addr_reg->address = v;
}

void reset_address_reg_latch(addr_reg *addr_reg)
{
    addr_reg->hi_addr = true;
}
