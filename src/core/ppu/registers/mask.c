#include <stdbool.h>
#include <stdint.h>

#include "core/ppu/register.h"

/*
    +-+-+-+-+-+-+-+-+
    |B|G|R|s|b|M|m|G|
    +-+-+-+-+-+-+-+-+
     | | | | | | | |
     | | | | | | | +- Greyscale
     | | | | | | |    (0: normal color, 1: produce a greyscale display)
     | | | | | | +--- 1: Show background in leftmost 8 pixels of screen,
     | | | | | |      0: Hide
     | | | | | +----- 1: Show sprites in leftmost 8 pixels of screen,
     | | | | |        0: Hide
     | | | | +------- 1: Show background
     | | | +--------- 1: Show sprites
     | | +----------- Emphasize red (green on PAL/Dendy)
     | +------------- Emphasize green (red on PAL/Dendy)
     +--------------- Emphasize blue
*/

bool is_greysacle(uint8_t reg)
{
    return (reg & 0x01) != 0;
}

bool leftmost_8px_background(uint8_t reg)
{
    return (reg & 0x02) != 0;
}

bool leftmost_8px_sprite(uint8_t reg)
{
    return (reg & 0x04) != 0;
}

bool show_background(uint8_t reg)
{
    return (reg & 0x08) != 0;
}

bool show_sprites(uint8_t reg)
{
    return (reg & 0x10) != 0;
}

bool emphasize(uint8_t reg, enum color c)
{
    return (reg & c) != 0;
}
