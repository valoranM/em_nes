#ifndef REGISTER_H
#define REGISTER_H

#include <stdbool.h>
#include <stdint.h>

// address

struct addr_reg_s
{
    bool hi_addr;
    uint16_t address;
    uint16_t tmp;
    uint16_t buff;
};

typedef struct addr_reg_s addr_reg;

#ifdef PPU

extern void update_address_reg(addr_reg *addr_reg, uint8_t address);
extern void increment_address_reg(addr_reg *addr_reg, uint8_t incr);
extern void reset_address_reg_latch(addr_reg *addr_reg);

// controller

extern uint8_t nametable_addr(uint8_t reg);
extern uint16_t increment_val(uint8_t reg);
extern uint16_t sprt_patterntable_addr(uint8_t reg);
extern uint16_t bknd_pattentable_addr(uint8_t reg);
extern uint8_t sprite_size(uint8_t reg);
extern uint8_t master_slave_select(uint8_t reg);
extern bool generate_vblank_nmi(uint8_t reg);

#endif // !PPU

// mask

enum color
{
    RED = 0x20,
    GREEN = 0x40,
    BLUE = 0x80,
};

#ifdef PPU

extern bool is_greysacle(uint8_t reg);
extern bool leftmost_8px_background(uint8_t reg);
extern bool leftmost_8px_sprite(uint8_t reg);
extern bool show_background(uint8_t reg);
extern bool show_sprites(uint8_t reg);
extern bool emphasize(uint8_t reg, enum color c);

#endif // !PPU

// scroll

struct scroll_reg_s
{
    uint8_t x_pos;
    uint8_t y_pos;
    bool latch;
};
typedef struct scroll_reg_s scroll_reg;

#ifdef PPU

extern void reset_scroll_reg_latch(scroll_reg *scroll_reg);
extern void update_scroll_reg(scroll_reg *scroll_reg, uint8_t data);

// status

extern void set_vblank_status(uint8_t *reg);
extern void set_sprite_zero_hit(uint8_t *reg);
extern void set_sprite_overflow(uint8_t *reg);
extern void reset_sprite_zero_hit(uint8_t *reg);
extern void reset_vblank_status(uint8_t *reg);
extern bool is_in_vblank(uint8_t reg);

#endif // !PPU

#endif // !REGISTER_H
