#ifndef VRAM_H
#define VRAM_H

#include "core/ppu/ppu.h"

/*
    +---------------+ $10000
    | Mirrors       |
    | $0000-$3FFF   |
    +---------------+ $4000
    | Mirrors       |
    | $3F00-$3F1F   |
    +---------------+ $3F00
    |               |
    | Palette       |
    |               |
    +---------------+ $3F00
    | Mirrors       |
    | $2000-$2EFF   |
    +---------------+ $3000
    |               |
    | Name Tables   |
    |               |
    +---------------+ $2000
    |               |
    | Pattern       |
    | Tables        |
    |               |
    +---------------+ $0000
*/

#define CHR_ROM_LIMIT 0x2000
#define NAME_TABLES_LIMIT 0x3F00
#define PALETTE_LIMIT 0x4000

extern uint8_t read_vram(ppu *ppu, uint16_t addr);
extern void write_vram(ppu *ppu, uint16_t addr, uint8_t val);

#endif // !VRAM_H
