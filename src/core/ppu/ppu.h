#ifndef PPU_H
#define PPU_H

#include <stdbool.h>
#include <stdint.h>

#include "core/card/rom.h"
#include "core/ppu/register.h"
#include "core/frame/frame.h"

#define PALETTE_SIZE 0x20
#define VRAM_SIZE 0x2000
#define OAM_SIZE 0x100

struct ppu_s
{
    uint8_t *chr_rom;
    uint8_t palettes[PALETTE_SIZE];
    uint8_t vram[VRAM_SIZE];
    uint16_t cycle;
    uint16_t scanline;

    uint8_t data_buf;
    bool NMI_interupt;

    frame *frame;

    enum Mirroring mirroring;

    // OAM
    uint8_t oam_data[OAM_SIZE];
    uint8_t oam_addr;

    // Registers
    uint8_t control;
    uint8_t mask;
    uint8_t status;
    scroll_reg scroll;
    addr_reg addr;
};

typedef struct ppu_s ppu;

extern ppu *init_ppu(rom *rom, frame *frame);
extern void free_ppu(ppu *ppu);
extern bool ppu_tick(ppu *ppu, uint8_t cycle);

// register.c file

extern uint16_t bknd_pattern_addr(ppu *ppu);
extern uint16_t sprt_pattern_addr(ppu *ppu);

// write
extern void write_to_ppu_ctrl(ppu *ppu, uint8_t data);
extern void write_to_ppu_mask(ppu *ppu, uint8_t data);
extern void write_to_oam_addr(ppu *ppu, uint8_t addr);
extern void write_to_oam_data(ppu *ppu, uint8_t data);
extern void write_to_ppu_scroll(ppu *ppu, uint8_t data);
extern void write_to_ppu_addr(ppu *ppu, uint8_t addr);
extern void write_to_ppu_data(ppu *ppu, uint8_t data);
extern void write_to_oam_dma(ppu *ppu, uint8_t data[256]);

// read
extern uint8_t read_ppu_status(ppu *ppu);
extern uint8_t read_oam_data(ppu *ppu);
extern uint8_t read_ppu_data(ppu *ppu);

#endif // !PPU_HDEBUG
