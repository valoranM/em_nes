#include <stdint.h>
#include <stdio.h>

#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"
#include "core/cpu/flags.h"
#include "core/cpu/instruction.h"

static void condition_branch(cpu *c, int8_t v, bool condition);
static void compare(cpu *c, uint8_t v1, uint8_t v2);
static uint8_t shift_l(cpu *c, uint8_t v);
static uint8_t shift_r(cpu *c, uint8_t v);
static uint8_t rot_r(cpu *c, uint8_t v);
static uint8_t rot_l(cpu *c, uint8_t v);

static void stack_push(cpu *c, uint8_t v);
static void stack_push_u16(cpu *c, uint16_t v);
static uint8_t stack_pop(cpu *c);
static uint16_t stack_pop_u16(cpu *c);

void branch(cpu *c, enum BranchInstr b)
{
    int8_t v = read_mem8(c->pc, c->bus);
    c->pc++;
    switch (b)
    {
    case BPL:
        condition_branch(c, v, !get_flag(c->p, NEGATIVE_F));
        break;
    case BMI:
        condition_branch(c, v, get_flag(c->p, NEGATIVE_F));
        break;
    case BVC:
        condition_branch(c, v, !get_flag(c->p, OVERFLOW_F));
        break;
    case BVS:
        condition_branch(c, v, get_flag(c->p, OVERFLOW_F));
        break;
    case BCC:
        condition_branch(c, v, !get_flag(c->p, CARRY_F));
        break;
    case BCS:
        condition_branch(c, v, get_flag(c->p, CARRY_F));
        break;
    case BNE:
        condition_branch(c, v, !get_flag(c->p, ZERO_F));
        break;
    case BEQ:
        condition_branch(c, v, get_flag(c->p, ZERO_F));
        break;
    }
}

static void condition_branch(cpu *c, int8_t v, bool condition)
{
    if (condition)
    {
        tick(c->bus, 1);
        uint16_t new_pc = c->pc + v;
        if ((c->pc & 0xFF00) != ((c->pc + v) & 0xFF00))
        {
            tick(c->bus, 1);
        }
        c->pc = new_pc;
    }
}

void stack(cpu *c, enum StackInstr instr)
{
    uint8_t p;
    switch (instr)
    {
    case TXS:
        c->sp = c->x;
        break;
    case TSX:
        c->p = clear_flag(c->p, NEGATIVE_F | ZERO_F);
        c->x = c->sp;
        c->p = set_flag(c->p, NZ_mask(c->x));
        break;
    case PHA:
        stack_push(c, c->a);
        break;
    case PLA:
        c->p = clear_flag(c->p, NEGATIVE_F | ZERO_F);
        c->a = stack_pop(c);
        c->p = set_flag(c->p, NZ_mask(c->a));
        break;
    case PHP:
        p = set_flag(c->p, BREAK_F | 0x20);
        stack_push(c, p);
        break;
    case PLP:
        c->p = clear_flag(stack_pop(c), BREAK_F);
        c->p = set_flag(c->p, 0x20);
        break;
    }
}

void register_instr(cpu *c, enum RegisterInstr instr)
{
    uint8_t reg_modif;
    switch (instr)
    {
    case TAX:
        c->x = c->a;
        reg_modif = c->x;
        break;
    case TXA:
        c->a = c->x;
        reg_modif = c->a;
        break;
    case DEX:
        c->x--;
        reg_modif = c->x;
        break;
    case INX:
        c->x++;
        reg_modif = c->x;
        break;
    case TAY:
        c->y = c->a;
        reg_modif = c->y;
        break;
    case TYA:
        c->a = c->y;
        reg_modif = c->a;
        break;
    case DEY:
        c->y--;
        reg_modif = c->y;
        break;
    case INY:
        c->y++;
        reg_modif = c->y;
        break;
    }
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(reg_modif));
}

void inst_flag(cpu *c, int clear, uint8_t flag)
{
    if (clear)
    {
        c->p = clear_flag(c->p, flag / 2);
    }
    else
    {
        c->p = set_flag(c->p, flag / 2);
    }
}

void brk(cpu *c)
{
    if (!get_flag(c->p, INTERRUPT_F))
    {
        c->pc++;
        stack_push_u16(c, c->pc);
        stack_push(c, c->p | (0x30 | BREAK_F));
        c->pc = read_mem16(IRQ_ADRESS, c->bus);
        c->p |= INTERRUPT_F;
    }
}

void nmi_interupt(cpu *c)
{
    stack_push_u16(c, c->pc);
    stack_push(c, c->p | (0x30 | BREAK_F));
    c->pc = read_mem16(NMI_ADRESS, c->bus);
    c->p |= INTERRUPT_F;
    tick(c->bus, 2);
}

void adc(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, true);
    uint8_t carry = get_flag(c->p, CARRY_F);
    uint16_t res = c->a + v + carry;

    c->p = clear_flag(c->p, CARRY_F | ZERO_F | NEGATIVE_F | OVERFLOW_F);
    uint8_t flags = NZ_mask(res);
    flags |= (res > 0xff) ? CARRY_F : 0;
    flags |= (~(c->a ^ v) & (c->a ^ res) & 0x80) ? OVERFLOW_F : 0;

    c->p = set_flag(c->p, flags);
    c->a = (uint8_t)res;
}

void and (cpu * c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, true);
    uint8_t res = c->a & v;

    c->p &= clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(res));
    c->a = res;
}

void asl(cpu *c, enum AddrMode addrmode)
{
    if (addrmode == Accumaltor)
    {
        c->a = shift_l(c, c->a);
    }
    else
    {
        uint16_t addr = get_addr(c->pc, addrmode, c);
        write_mem8(addr, shift_l(c, read_mem8(addr, c->bus)), c->bus);
    }
}

void bit(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, false);
    uint8_t z = c->a & v;

    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F | OVERFLOW_F);
    c->p = set_flag(c->p, v & 0xc0);
    c->p |= (z == 0) ? ZERO_F : 0;
}

void cmp(cpu *c, enum AddrMode addrmode)
{
    compare(c, c->a, get_param(c->pc, addrmode, c, true));
}

void cpx(cpu *c, enum AddrMode addrmode)
{
    compare(c, c->x, get_param(c->pc, addrmode, c, false));
}

void cpy(cpu *c, enum AddrMode addrmode)
{
    compare(c, c->y, get_param(c->pc, addrmode, c, false));
}

void dec(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t v = read_mem8(addr, c->bus) - 1;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    write_mem8(addr, v, c->bus);
}

void inc(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t v = read_mem8(addr, c->bus) + 1;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    write_mem8(addr, v, c->bus);
}

void eor(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = c->a ^ get_param(c->pc, addrmode, c, true);
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    c->a = v;
}

void jmp(cpu *c, enum AddrMode addrmode)
{
    uint16_t dest = get_addr(c->pc, addrmode, c);
    c->pc = dest;
}

void lda(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, true);
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    c->a = v;
}

void ldx(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, true);
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    c->x = v;
}

void ldy(cpu *c, enum AddrMode addrmode)
{
    uint8_t v = get_param(c->pc, addrmode, c, true);
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(v));
    c->y = v;
}

void sta(cpu *c, enum AddrMode addrmode)
{
    uint16_t v = get_addr(c->pc, addrmode, c);
    write_mem8(v, c->a, c->bus);
}

void stx(cpu *c, enum AddrMode addrmode)
{
    uint16_t v = get_addr(c->pc, addrmode, c);
    write_mem8(v, c->x, c->bus);
}

void sty(cpu *c, enum AddrMode addrmode)
{
    uint16_t v = get_addr(c->pc, addrmode, c);
    write_mem8(v, c->y, c->bus);
}

void jsr(cpu *c)
{
    stack_push_u16(c, c->pc + 2 - 1);
    uint16_t target = read_mem16(c->pc, c->bus);
    c->pc = target;
}

void rts(cpu *c)
{
    c->pc = stack_pop_u16(c) + 1;
}

void nop(cpu *c, enum AddrMode addrmode)
{
    get_param(c->pc, addrmode, c, true);
}

void lsr(cpu *c, enum AddrMode addrmode)
{
    if (addrmode == Accumaltor)
    {
        c->a = shift_r(c, c->a);
    }
    else
    {
        uint16_t addr = get_addr(c->pc, addrmode, c);
        write_mem8(addr, shift_r(c, read_mem8(addr, c->bus)), c->bus);
    }
}

void ora(cpu *c, enum AddrMode addrmode)
{
    uint8_t param = get_param(c->pc, addrmode, c, true);
    c->a = param | c->a;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(c->a));
}

void sbc(cpu *c, enum AddrMode addrmode)
{
    uint8_t param = get_param(c->pc, addrmode, c, true);
    uint8_t carry = get_flag(c->p, CARRY_F) == 0;
    uint16_t diff = c->a - param - carry;

    c->p = clear_flag(c->p, CARRY_F | NEGATIVE_F | OVERFLOW_F | CARRY_F);
    uint8_t flags = NZ_mask(diff);
    flags |= (diff <= 0xff) ? CARRY_F : 0;
    flags |= ((c->a ^ diff) & (~param ^ diff) & 0x80) ? OVERFLOW_F : 0;

    c->p = set_flag(c->p, flags);
    c->a = (uint8_t)diff;
}

void rol(cpu *c, enum AddrMode addrmode)
{
    if (addrmode == Accumaltor)
    {
        c->a = rot_l(c, c->a);
    }
    else
    {
        uint16_t addr = get_addr(c->pc, addrmode, c);
        write_mem8(addr, rot_l(c, read_mem8(addr, c->bus)), c->bus);
    }
}

void ror(cpu *c, enum AddrMode addrmode)
{
    if (addrmode == Accumaltor)
    {
        c->a = rot_r(c, c->a);
    }
    else
    {
        uint16_t addr = get_addr(c->pc, addrmode, c);
        write_mem8(addr, rot_r(c, read_mem8(addr, c->bus)), c->bus);
    }
}

void rti(cpu *c)
{
    c->p &= (BREAK_F | 0x20);
    c->p |= stack_pop(c) & ~(BREAK_F | 0x20);
    c->pc = stack_pop_u16(c);
}

// unofficial

void alr(cpu *c, enum AddrMode addrmode)
{
    c->a &= get_param(c->pc, addrmode, c, false);
    c->a = shift_r(c, c->a);
}

void anc(cpu *c, enum AddrMode addrmode)
{
    c->a &= get_param(c->pc, addrmode, c, false);
    c->p = clear_flag(c->p, CARRY_F | ZERO_F | NEGATIVE_F);
    uint8_t flags = (c->a & 0x80) ? (CARRY_F | NEGATIVE_F) : 0;
    flags |= (c->a == 0) ? ZERO_F : 0;
    c->p = set_flag(c->p, flags);
}

void arr(cpu *c, enum AddrMode addrmode)
{
    uint8_t val = c->a & get_param(c->pc, addrmode, c, false);
    c->a = rot_r(c, val);
}

void axs(cpu *c, enum AddrMode addrmode)
{
    uint8_t param = get_param(c->pc, addrmode, c, false);
    c->x = c->x & c->a;
    compare(c, c->x, param);
}

void lax(cpu *c, enum AddrMode addrmode)
{
    c->a = get_param(c->pc, addrmode, c, true);
    c->x = c->a;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(c->a));
}

void sax(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    write_mem8(addr, c->x & c->a, c->bus);
}

void dcp(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t param = read_mem8(addr, c->bus) - 1;
    compare(c, c->a, param);
    write_mem8(addr, param, c->bus);
}

void isb(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t param = read_mem8(addr, c->bus) + 1;
    write_mem8(addr, param, c->bus);
    uint16_t diff = c->a - param - ((c->p & CARRY_F) == 0);

    c->p = clear_flag(c->p, CARRY_F | NEGATIVE_F | OVERFLOW_F | CARRY_F);
    uint8_t flags = NZ_mask(diff);
    flags |= (diff <= 0xff) ? CARRY_F : 0;
    flags |= ((c->a ^ diff) & (~param ^ diff) & 0x80) ? OVERFLOW_F : 0;

    c->p = set_flag(c->p, flags);
    c->a = diff;
}

void slo(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t m = shift_l(c, read_mem8(addr, c->bus));
    write_mem8(addr, m, c->bus);
    c->a |= m;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(c->a));
}

void sre(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t m = shift_r(c, read_mem8(addr, c->bus));
    write_mem8(addr, m, c->bus);
    c->a ^= m;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(c->a));
}

void rla(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t m = rot_l(c, read_mem8(addr, c->bus));
    write_mem8(addr, m, c->bus);
    c->a &= m;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F);
    c->p = set_flag(c->p, NZ_mask(c->a));
}

void rra(cpu *c, enum AddrMode addrmode)
{
    uint16_t addr = get_addr(c->pc, addrmode, c);
    uint8_t m = rot_r(c, read_mem8(addr, c->bus));
    write_mem8(addr, m, c->bus);
    uint8_t carry = get_flag(c->p, CARRY_F);
    uint16_t res = c->a + m + carry;

    c->p = clear_flag(c->p, CARRY_F | ZERO_F | NEGATIVE_F | OVERFLOW_F);
    uint8_t flags = NZ_mask(res);
    flags |= (res > 0xff) ? CARRY_F : 0;
    flags |= (~(c->a ^ m) & (c->a ^ res) & 0x80) ? OVERFLOW_F : 0;

    c->p = set_flag(c->p, flags);
    c->a = (uint8_t)res;
}

static void compare(cpu *c, uint8_t v1, uint8_t v2)
{
    uint16_t sub = v1 - v2;
    c->p = clear_flag(c->p, ZERO_F | NEGATIVE_F | CARRY_F);
    c->p = set_flag(c->p, NZ_mask(sub & 0xff));
    c->p |= (v1 >= v2) ? CARRY_F : 0;
}

static uint8_t shift_l(cpu *c, uint8_t v)
{
    c->p = clear_flag(c->p, NEGATIVE_F | CARRY_F | ZERO_F);
    c->p |= ((v & 0x80) != 0) ? CARRY_F : 0;
    v <<= 1;
    c->p = set_flag(c->p, NZ_mask(v));
    return v;
}

static uint8_t shift_r(cpu *c, uint8_t v)
{
    c->p = clear_flag(c->p, NEGATIVE_F | CARRY_F | ZERO_F);
    c->p |= ((v & 0x1) != 0) ? CARRY_F : 0;
    v >>= 1;
    c->p = set_flag(c->p, NZ_mask(v));
    return v;
}

static uint8_t rot_l(cpu *c, uint8_t v)
{
    uint8_t res = v << 1;
    res |= get_flag(c->p, CARRY_F);
    c->p = clear_flag(c->p, NEGATIVE_F | ZERO_F | CARRY_F);
    c->p = set_flag(c->p, ((v & 0x80) != 0) ? CARRY_F : 0);
    c->p = set_flag(c->p, NZ_mask(res));
    return res;
}

static uint8_t rot_r(cpu *c, uint8_t v)
{
    uint8_t res = v >> 1;
    res |= get_flag(c->p, CARRY_F) << 7;
    c->p = clear_flag(c->p, NEGATIVE_F | ZERO_F | CARRY_F);
    c->p = set_flag(c->p, ((v & 0x01) != 0) ? CARRY_F : 0);
    c->p = set_flag(c->p, NZ_mask(res));
    return res;
}

static void stack_push(cpu *c, uint8_t v)
{
    uint16_t addr = STACK_END + c->sp;
    write_mem8(addr, v, c->bus);
    c->sp--;
}

static void stack_push_u16(cpu *c, uint16_t v)
{
    uint8_t lo = v & 0xff;
    uint8_t hi = v >> 8;
    stack_push(c, hi);
    stack_push(c, lo);
}

static uint8_t stack_pop(cpu *c)
{
    c->sp++;
    return read_mem8(c->sp + STACK_END, c->bus);
}

static uint16_t stack_pop_u16(cpu *c)
{
    uint8_t lo = stack_pop(c);
    uint8_t hi = stack_pop(c);
    return lo | hi << 8;
}
