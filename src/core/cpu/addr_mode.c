#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"

static void has_break_addrmode(cpu *c, uint16_t addr, enum AddrMode addrmode);

uint8_t get_param(uint16_t addr, enum AddrMode mode, cpu *c, bool b)
{
    uint16_t a;
    switch (mode)
    {
    case Implied:
    case None:
        return 0;
    case Accumaltor:
        return c->a;
    default:
        a = get_addr(addr, mode, c);
        if (b)
        {
            has_break_addrmode(c, a, mode);
        }
        return read_mem8(a, c->bus);
    }
}

uint16_t get_addr(uint16_t addr, enum AddrMode mode, cpu *c)
{
    uint8_t hi, lo;
    switch (mode)
    {
    case Immediate:
        c->pc += 1;
        return addr;
    case Zero_Page:
        c->pc += 1;
        return read_mem8(addr, c->bus) & 0xff;
    case Absolute:
        c->pc += 2;
        return read_mem16(addr, c->bus);
    case Indirect_Abs:
        c->pc += 2;
        addr = read_mem16(addr, c->bus);
        // 6502 bug
        //            Lower   Higer
        // $(12FF) => $(12FF) $(1200)
        lo = read_mem8(addr, c->bus);
        hi = read_mem8((addr & 0xFF00) | ((addr + 1) & 0xFF), c->bus);
        return (hi << 8) | lo;
    case Ab_Indexed_X:
        c->pc += 2;
        return read_mem16(addr, c->bus) + c->x;
    case Ab_Indexed_Y:
        c->pc += 2;
        return read_mem16(addr, c->bus) + c->y;
    case Zero_Page_X:
        c->pc += 1;
        return (read_mem8(addr, c->bus) + c->x) & 0xff;
    case Zero_Page_Y:
        c->pc += 1;
        return (read_mem8(addr, c->bus) + c->y) & 0xff;
    case Indexed_Indirect:
        c->pc += 1;
        return read_mem16_u8addr(read_mem8(addr, c->bus) + c->x, c->bus);
    case Indirect_Indexed:
        c->pc += 1;
        return read_mem16_u8addr(read_mem8(addr, c->bus), c->bus) + c->y;
    case Relative:
        c->pc += 1;
        return (uint16_t)(c->pc + (int8_t)read_mem8(addr, c->bus));

    default:
        return 0;
    }
}

static void has_break(cpu *c, uint16_t a1, uint16_t a2);

static void has_break_addrmode(cpu *c, uint16_t addr, enum AddrMode addrmode)
{
    switch (addrmode)
    {
    case Ab_Indexed_X:
        has_break(c, addr, addr - c->x);
        break;
    case Ab_Indexed_Y:
    case Indirect_Indexed:
        has_break(c, addr, addr - c->y);
        break;
    default:
        break;
    }
}

static void has_break(cpu *c, uint16_t a1, uint16_t a2)
{
    if ((a1 & 0xFF00) != (a2 & 0xFF00))
    {
        tick(c->bus, 1);
        // c->bus->cycle++;
    }
}
