#ifndef ADDR_MODE_H
#define ADDR_MODE_H

#include <stdbool.h>
#include <stdint.h>

#include "core/cpu/cpu.h"

enum AddrMode
{
    None,
    Implied,
    Accumaltor,
    Immediate,
    Absolute,
    Zero_Page,
    Zero_Page_X, // oper, X
    Zero_Page_Y, // oper, Y
    Ab_Indexed_X,
    Ab_Indexed_Y,
    Indirect_Abs,
    Indexed_Indirect, // (oper, X)
    Indirect_Indexed, // (oper), Y
    Relative,
};

/*
 * return instruction paramter
 * @param uint16_t addresse
 * @param enum AddrMode addresse mode
 * @param cpu* cpu addresse
 * @param bool cycle count page break
 * @retun new addresse
 */
extern uint8_t get_param(uint16_t addr, enum AddrMode mode, cpu *c, bool b);

/*
 * return addr in terms of add mode
 * @param uint16_t addresse
 * @param enum AddrMode addresse mode
 * @param cpu* cpu addresse
 * @retun new addresse
 */
extern uint16_t get_addr(uint16_t addr, enum AddrMode mode, cpu *c);

#endif // ADDR_MODE_H
