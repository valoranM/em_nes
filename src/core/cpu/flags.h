#ifndef P_STATUS

#include <stdint.h>

/*
      P Register:
      -----------

     7 6 5 4 3 2 1 0
    +-+-+-+-+-+-+-+-+
    |N|V| |B|D|I|Z|C|
    +-+-+-+-+-+-+-+-+
     | |   | | | | +- CARRY_F
     | |   | | | +--- ZERO_F
     | |   | | +----- INTERRUPT_F
     | |   | +------- DECIMAL_F
     | |   +--------- BREAK_F
     | +------------- OVERFLOW_F
     +--------------- NEGATIVE_F
*/

#define CARRY_F 0x01
#define ZERO_F 0x02
#define INTERRUPT_F 0x04
#define DECIMAL_F 0x08
#define BREAK_F 0x10
#define OVERFLOW_F 0x40
#define NEGATIVE_F 0x80

/*
 * Get flag in register P
 * @param uint8_t register
 * @param uint8_t flag
 * @return int (if flag is set)
 */
extern int get_flag(uint8_t reg, uint8_t flag);

/*
 * Set flag in register P
 * @param uint8_t register
 * @param uint8_t flag
 * @return uint8_t (register with flag acctivated)
 */
extern uint8_t set_flag(uint8_t reg, uint8_t flags);

/*
 * Clear flag in register P
 * @param uint8_t register
 * @param uint8_t flag
 * @return uint8_t (register with flag cleared)
 */
extern uint8_t clear_flag(uint8_t reg, uint8_t flags);

/*
 * Make a masc for NZ flag
 * @param uint8_t value
 * @return uint8_t mask for set NZ flag
 */
extern uint8_t NZ_mask(uint8_t val);


#endif // P_STATUS
