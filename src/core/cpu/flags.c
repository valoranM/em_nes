#include "core/cpu/flags.h"

/*
    0x00_0010
  & 0100_0000
    ---------
  r:0x00_0000

if r > 0  then x == 1
          else x == 0
*/
int get_flag(uint8_t reg, uint8_t flag)
{
    return (flag & reg) > 0;
}

/*
    0000_1000 | 1000_0000 => 1000_1000
*/
uint8_t set_flag(uint8_t reg, uint8_t flags)
{
    return flags | reg;
}

/*
    0100_0000
  ~ 1011_1111 & 0100_0101 => 0000_0101
*/
uint8_t clear_flag(uint8_t reg, uint8_t flags)
{
    return reg & ~flags;
}

uint8_t NZ_mask(uint8_t val)
{
    uint8_t res = (val == 0) ? ZERO_F : 0x0;
    res = res | (val & NEGATIVE_F);
    return res;
}
