#include <stdint.h>

#include "core/cpu/opcode.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/instruction.h"

#define OP(OP, CYC, ARG)   {(OP),      #OP, CYC, ARG, {0}}
#define OP_BRANCH(OP, CYC) {BRANCH,    #OP, CYC, Relative,.branch=OP}
#define OP_REGS(OP, CYC)   {REGISTER,  #OP, CYC, Implied, .reg=OP}
#define OP_STACK(OP, CYC)  {STACK,     #OP, CYC, Implied, .stack=OP}
#define OP_FLAGS(OP, CYC)  {FLAGS,     #OP, CYC, Implied, .flag=OP}

#define NIL_OP {NOP, "NOP", 2, None, {0}}

static struct opcode opcodes[] = 
  {
    OP(BRK, 7, Implied),     OP(ORA, 6, Indexed_Indirect),  NIL_OP,                    OP(SLO, 8, Indexed_Indirect),  // 0x04
    OP(NOP, 3, Zero_Page),   OP(ORA, 3, Zero_Page),         OP(ASL, 5,  Zero_Page),    OP(SLO, 5, Zero_Page),         // 0x08
    OP_STACK(PHP, 3),        OP(ORA, 2, Immediate),         OP(ASL, 2,  Accumaltor),   OP(ANC, 2, Immediate),         // 0x0C
    OP(NOP, 4, Absolute),    OP(ORA, 4, Absolute),          OP(ASL, 6,  Absolute),     OP(SLO, 6, Absolute),          // 0x10
    OP_BRANCH(BPL, 2),       OP(ORA, 5, Indirect_Indexed),  NIL_OP,                    OP(SLO, 8, Indirect_Indexed),  // 0x14
    OP(NOP, 4, Zero_Page_X), OP(ORA, 4, Zero_Page_X),       OP(ASL, 6, Zero_Page_X),   OP(SLO, 6, Zero_Page_X),       // 0x18
    OP_FLAGS(CLC, 2),        OP(ORA, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(SLO, 7, Ab_Indexed_Y),      // 0x1C
    OP(NOP, 4, Ab_Indexed_X),OP(ORA, 4, Ab_Indexed_X),      OP(ASL, 7, Ab_Indexed_X),  OP(SLO, 7, Ab_Indexed_X),      // 0x20
    OP(JSR, 6, Absolute),    OP(AND, 6, Indexed_Indirect),  NIL_OP,                    OP(RLA, 8, Indexed_Indirect),  // 0x24 
    OP(BIT, 3, Zero_Page),   OP(AND, 3, Zero_Page),         OP(ROL, 5, Zero_Page),     OP(RLA, 5, Zero_Page),         // 0x28 
    OP_STACK(PLP, 4),        OP(AND, 2, Immediate),         OP(ROL, 2, Accumaltor),    OP(ANC, 2, Immediate),         // 0x2C 
    OP(BIT, 4, Absolute),    OP(AND, 4, Absolute),          OP(ROL, 6, Absolute),      OP(RLA, 6, Absolute),          // 0x30
    OP_BRANCH(BMI, 2),       OP(AND, 5, Indirect_Indexed),  NIL_OP,                    OP(RLA, 8, Indirect_Indexed),  // 0x34
    OP(NOP, 4, Zero_Page_X), OP(AND, 4, Zero_Page_X),       OP(ROL, 6, Zero_Page_X),   OP(RLA, 6, Zero_Page_X),       // 0x38 
    OP_FLAGS(SEC, 2),        OP(AND, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(RLA, 7, Ab_Indexed_Y),      // 0x3C 
    OP(NOP, 4, Ab_Indexed_X),OP(AND, 4, Ab_Indexed_X),      OP(ROL, 7, Ab_Indexed_X),  OP(RLA, 7, Ab_Indexed_X),      // 0x40 
    OP(RTI, 6, Implied),     OP(EOR, 6, Indexed_Indirect),  NIL_OP,                    OP(SRE, 8, Indexed_Indirect),  // 0x44 
    OP(NOP, 3, Zero_Page),   OP(EOR, 3, Zero_Page),         OP(LSR, 5, Zero_Page),     OP(SRE, 5, Zero_Page),         // 0x48 
    OP_STACK(PHA, 3),        OP(EOR, 2, Immediate),         OP(LSR, 2, Accumaltor),    OP(ALR, 2, Immediate),         // 0x4C 
    OP(JMP, 3, Absolute),    OP(EOR, 4, Absolute),          OP(LSR, 6, Absolute),      OP(SRE, 6, Absolute),          // 0x50 
    OP_BRANCH(BVC, 2),       OP(EOR, 5, Indirect_Indexed),  NIL_OP,                    OP(SRE, 8, Indirect_Indexed),  // 0x54 
    OP(NOP, 4, Zero_Page_X), OP(EOR, 4, Zero_Page_X),       OP(LSR, 6, Zero_Page_X),   OP(SRE, 6, Zero_Page_X),       // 0x58 
    OP_FLAGS(CLI, 2),        OP(EOR, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(SRE, 7, Ab_Indexed_Y),      // 0x5C 
    OP(NOP, 4, Ab_Indexed_X),OP(EOR, 4, Ab_Indexed_X),      OP(LSR, 7, Ab_Indexed_X),  OP(SRE, 7, Ab_Indexed_X),      // 0x60 
    OP(RTS, 6, Implied),     OP(ADC, 6, Indexed_Indirect),  NIL_OP,                    OP(RRA, 8, Indexed_Indirect),  // 0x64 
    OP(NOP, 3, Zero_Page),   OP(ADC, 3, Zero_Page),         OP(ROR, 5, Zero_Page),     OP(RRA, 5, Zero_Page),         // 0x68 
    OP_STACK(PLA, 4),        OP(ADC, 2, Immediate),         OP(ROR, 2, Accumaltor),    OP(ARR, 2, Immediate),         // 0x6C 
    OP(JMP, 5, Indirect_Abs),OP(ADC, 4, Absolute),          OP(ROR, 6, Absolute),      OP(RRA, 6, Absolute),          // 0x70
    OP_BRANCH(BVS, 2),       OP(ADC, 5, Indirect_Indexed),  NIL_OP,                    OP(RRA, 8, Indirect_Indexed),  // 0x74 
    OP(NOP, 4, Zero_Page_X), OP(ADC, 4, Zero_Page_X),       OP(ROR, 6, Zero_Page_X),   OP(RRA, 6, Zero_Page_X),       // 0x78 
    OP_FLAGS(SEI, 2),        OP(ADC, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(RRA, 7, Ab_Indexed_Y),      // 0x7C 
    OP(NOP, 4, Ab_Indexed_X),OP(ADC, 4, Ab_Indexed_X),      OP(ROR, 7, Ab_Indexed_X),  OP(RRA, 7, Ab_Indexed_X),      // 0x80
    OP(NOP, 2, Immediate),   OP(STA, 6, Indexed_Indirect),  OP(NOP, 2, Immediate),     OP(SAX, 6, Indexed_Indirect),  // 0x84
    OP(STY, 3, Zero_Page),   OP(STA, 3, Zero_Page),         OP(STX, 3, Zero_Page),     OP(SAX, 3, Zero_Page),         // 0x88
    OP_REGS(DEY, 2),         OP(NOP, 2, Immediate),         OP_REGS(TXA, 2),           OP(STY, 4, Absolute),          // 0x8C
    OP(STY, 4, Absolute),    OP(STA, 4, Absolute),          OP(STX, 4, Absolute),      OP(SAX, 4, Absolute),          // 0x90
    OP_BRANCH(BCC, 2),       OP(STA, 6, Indirect_Indexed),  NIL_OP,                    NIL_OP,                        // 0x94
    OP(STY, 4, Zero_Page_X), OP(STA, 4, Zero_Page_X),       OP(STX, 4, Zero_Page_Y),   OP(SAX, 4, Zero_Page_Y),       // 0x98
    OP_REGS(TYA, 2),         OP(STA, 5, Ab_Indexed_Y),      OP_STACK(TXS, 2),          NIL_OP,                        // 0x9C
    NIL_OP,                  OP(STA, 5, Ab_Indexed_X),      OP(LDX, 4, Absolute),      NIL_OP,                        // 0xA0
    OP(LDY, 2, Immediate),   OP(LDA, 6, Indexed_Indirect),  OP(LDX, 2, Immediate),     OP(LAX, 6, Indexed_Indirect),  // 0xA4
    OP(LDY, 3, Zero_Page),   OP(LDA, 3, Zero_Page),         OP(LDX, 3, Zero_Page),     OP(LAX, 3, Zero_Page),         // 0xA8
    OP_REGS(TAY, 2),         OP(LDA, 2, Immediate),         OP_REGS(TAX, 2),           NIL_OP,                        // 0xAC 
    OP(LDY, 4, Absolute),    OP(LDA, 4, Absolute),          OP(LDX, 4, Absolute),      OP(LAX, 4, Absolute),          // 0xB0
    OP_BRANCH(BCS, 2),       OP(LDA, 5, Indirect_Indexed),  NIL_OP,                    OP(LAX, 5, Indirect_Indexed),  // 0xB4
    OP(LDY, 4, Zero_Page_X), OP(LDA, 4, Zero_Page_X),       OP(LDX, 4, Zero_Page_Y),   OP(LAX, 4, Zero_Page_Y),       // 0xB8
    OP_FLAGS(CLV, 2),        OP(LDA, 4, Ab_Indexed_Y),      OP_STACK(TSX, 2),          OP(LAX, 4, Ab_Indexed_Y),      // 0xBC
    OP(LDY, 4, Ab_Indexed_X),OP(LDA, 4, Ab_Indexed_X),      OP(LDX, 4, Ab_Indexed_Y),  OP(LAX, 4, Ab_Indexed_Y),      // 0xC0
    OP(CPY, 2, Immediate),   OP(CMP, 6, Indexed_Indirect),  OP(NOP, 2, Immediate),     OP(DCP, 8, Indexed_Indirect),  // 0xC4
    OP(CPY, 3, Zero_Page),   OP(CMP, 3, Zero_Page),         OP(DEC, 5, Zero_Page),     OP(DCP, 5, Zero_Page),         // 0xC8
    OP_REGS(INY, 2),         OP(CMP, 2, Immediate),         OP_REGS(DEX, 2),           OP(AXS, 2, Immediate),         // 0xCC
    OP(CPY, 4, Absolute),    OP(CMP, 4, Absolute),          OP(DEC, 6, Absolute),      OP(DCP, 6, Absolute),          // 0xD0
    OP_BRANCH(BNE, 2),       OP(CMP, 5, Indirect_Indexed),  NIL_OP,                    OP(DCP, 8, Indirect_Indexed),  // 0xD4
    OP(NOP, 4, Zero_Page_X), OP(CMP, 4, Zero_Page_X),       OP(DEC, 6, Zero_Page_X),   OP(DCP, 6, Zero_Page_X),       // 0xD8
    OP_FLAGS(CLD, 2),        OP(CMP, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(DCP, 7, Ab_Indexed_Y),      // 0xDC
    OP(NOP, 4, Ab_Indexed_X),OP(CMP, 4, Ab_Indexed_X),      OP(DEC, 7, Ab_Indexed_X),  OP(DCP, 7, Ab_Indexed_X),      // 0xE0
    OP(CPX, 2, Immediate),   OP(SBC, 6, Indexed_Indirect),  OP(NOP, 2, Immediate),     OP(ISB, 8, Indexed_Indirect),  // 0xE4
    OP(CPX, 3, Zero_Page),   OP(SBC, 3, Zero_Page),         OP(INC, 5, Zero_Page),     OP(ISB, 5, Zero_Page),         // 0xE8
    OP_REGS(INX, 2),         OP(SBC, 2, Immediate),         OP(NOP, 2, None),          OP(SBC, 2, Immediate),         // 0xEC
    OP(CPX, 4, Absolute),    OP(SBC, 4, Absolute),          OP(INC, 6, Absolute),      OP(ISB, 6, Absolute),          // 0xF0
    OP_BRANCH(BEQ, 2),       OP(SBC, 5, Indirect_Indexed),  NIL_OP,                    OP(ISB, 8, Indirect_Indexed),  // 0xF4
    OP(NOP, 4, Zero_Page_X), OP(SBC, 4, Zero_Page_X),       OP(INC, 6, Zero_Page_X),   OP(ISB, 6, Zero_Page_X),       // 0xF8
    OP_FLAGS(SED, 2),        OP(SBC, 4, Ab_Indexed_Y),      OP(NOP, 2, Implied),       OP(ISB, 7, Ab_Indexed_Y),      // 0xFC
    OP(NOP, 4, Ab_Indexed_X),OP(SBC, 4, Ab_Indexed_X),      OP(INC, 7, Ab_Indexed_X),  OP(ISB, 7, Ab_Indexed_X),
};

struct opcode get_opcode(uint8_t hex)
{
    return opcodes[hex];
}
