#ifndef CPU_H
#define CPU_H

#include <stdint.h>

#include "core/bus/bus.h"

struct s_cpu
{
    uint8_t x;
    uint8_t y;
    uint8_t a;
    uint8_t p;
    uint8_t sp;
    uint16_t pc;

    bus *bus;
};

typedef struct s_cpu cpu;

extern cpu *init_cpu(bus *bus);

extern void free_cpu(cpu *c);

extern void reset_cpu(cpu *c);

extern void run(cpu *cpu);

extern bool execute(cpu *c);

#endif // CPU_H
