#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"

#define STACK_END 0x100

enum BranchInstr
{
    BPL,
    BMI,
    BVC,
    BVS,
    BCC,
    BCS,
    BNE,
    BEQ,
};

enum RegisterInstr
{
    TAX,
    TXA,
    DEX,
    INX,
    TAY,
    TYA,
    DEY,
    INY,
};

enum StackInstr
{
    TXS,
    TSX,
    PHA,
    PLA,
    PHP,
    PLP,
};

/*
    The modified flag is multiplied by 2 to have two cases:
      - even: set to 1
      - odd:  set to 0
    We just have to divide by 2 to get the flag to modify
*/
enum FlagsInstr
{
    CLC = 0x02,
    SEC = 0x03,
    CLI = 0x08,
    SEI = 0x09,
    CLV = 0x80,
    CLD = 0x10,
    SED = 0x11,
};

extern void branch(cpu *c, enum BranchInstr b);
extern void stack(cpu *c, enum StackInstr instr);
extern void register_instr(cpu *c, enum RegisterInstr instr);
extern void inst_flag(cpu *c, int clear, uint8_t flag);
extern void brk(cpu *c);
extern void nmi_interupt(cpu *c);
extern void adc(cpu *c, enum AddrMode addrmode);
extern void and (cpu * c, enum AddrMode addrmode);
extern void asl(cpu *c, enum AddrMode addrmode);
extern void bit(cpu *c, enum AddrMode addrmode);
extern void cmp(cpu *c, enum AddrMode addrmode);
extern void cpx(cpu *c, enum AddrMode addrmode);
extern void cpy(cpu *c, enum AddrMode addrmode);
extern void dec(cpu *c, enum AddrMode addrmode);
extern void inc(cpu *c, enum AddrMode addrmode);
extern void eor(cpu *c, enum AddrMode addrmode);
extern void jmp(cpu *c, enum AddrMode addrmode);
extern void lda(cpu *c, enum AddrMode addrmode);
extern void ldx(cpu *c, enum AddrMode addrmode);
extern void ldy(cpu *c, enum AddrMode addrmode);
extern void sta(cpu *c, enum AddrMode addrmode);
extern void stx(cpu *c, enum AddrMode addrmode);
extern void sty(cpu *c, enum AddrMode addrmode);
extern void jsr(cpu *c);
extern void rts(cpu *c);
extern void nop(cpu *c, enum AddrMode addrmode);
extern void lsr(cpu *c, enum AddrMode addrmode);
extern void ora(cpu *c, enum AddrMode addrmode);
extern void sbc(cpu *c, enum AddrMode addrmode);
extern void rol(cpu *c, enum AddrMode addrmode);
extern void ror(cpu *c, enum AddrMode addrmode);
extern void rti(cpu *c);

// unofficial

extern void alr(cpu *c, enum AddrMode addrmode);
extern void anc(cpu *c, enum AddrMode addrmode);
extern void arr(cpu *c, enum AddrMode addrmode);
extern void lax(cpu *c, enum AddrMode addrmode);
extern void sax(cpu *c, enum AddrMode addrmode);
extern void dcp(cpu *c, enum AddrMode addrmode);
extern void isb(cpu *c, enum AddrMode addrmode);
extern void slo(cpu *c, enum AddrMode addrmode);
extern void sre(cpu *c, enum AddrMode addrmode);
extern void rla(cpu *c, enum AddrMode addrmode);
extern void rra(cpu *c, enum AddrMode addrmode);

enum Instruction
{
    BRANCH,
    REGISTER,
    STACK,
    FLAGS,
    ADC,
    AND,
    ASL,
    BIT,
    BRK,
    CMP,
    CPX,
    CPY,
    DEC,
    EOR,
    INC,
    JMP,
    JSR,
    LDA,
    LDX,
    LDY,
    LSR,
    NOP,
    ORA,
    ROL,
    ROR,
    RTI,
    RTS,
    SBC,
    STA,
    STX,
    STY,
    // unofficial
    ALR,
    ANC,
    ARR,
    AXS,
    LAX,
    SAX,
    DCP,
    ISB,
    RLA,
    RRA,
    SLO,
    SRE,
    SKB,
    IGN,
};
#endif // INSTRUCTION_H
