#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/bus/bus.h"
#include "core/cpu/cpu.h"
#include "core/cpu/instruction.h"
#include "core/cpu/opcode.h"
#include "utils/debug.h"

static void check_nmi_state(cpu *c);

cpu *init_cpu(bus *bus)
{
    cpu *res = (cpu *)malloc(sizeof(cpu));
    memset(res, 0, sizeof(cpu));
    res->sp = 0xfd;
    res->p = 0x24;
    res->pc = 0xC000;
    res->bus = bus;
    return res;
}

void free_cpu(cpu *c)
{
    free(c);
}

void reset_cpu(cpu *c)
{
    // tick(c->bus, 7);
    c->pc = read_mem16(RESET_ADDRESS, c->bus);
}

void run(cpu *cpu)
{
    while (1)
    {
        if (cpu->bus->ppu->NMI_interupt)
        {
            cpu->bus->ppu->NMI_interupt = false;
            nmi_interupt(cpu);
        }
        if (execute(cpu))
        {
            break;
        }
    }
}

bool execute(cpu *c)
{
    check_nmi_state(c);
    // PRINT_CPU_STATE(c);
    uint8_t hex = read_mem8(c->pc, c->bus);
    c->pc++;
    struct opcode code = get_opcode(hex);

    switch (code.opcode)
    {
    case BRANCH:
        branch(c, code.branch);
        break;
    case REGISTER:
        register_instr(c, code.reg);
        break;
    case STACK:
        stack(c, code.stack);
        break;
    case FLAGS:
        inst_flag(c, (code.flag % 2) == 0, code.flag);
        break;
    case ADC:
        adc(c, code.addrmode);
        break;
    case AND:
        and(c, code.addrmode);
        break;
    case ASL:
        asl(c, code.addrmode);
        break;
    case BIT:
        bit(c, code.addrmode);
        break;
    case BRK:
        brk(c);
        break;
    case CMP:
        cmp(c, code.addrmode);
        break;
    case CPX:
        cpx(c, code.addrmode);
        break;
    case CPY:
        cpy(c, code.addrmode);
        break;
    case DEC:
        dec(c, code.addrmode);
        break;
    case EOR:
        eor(c, code.addrmode);
        break;
    case INC:
        inc(c, code.addrmode);
        break;
    case JMP:
        jmp(c, code.addrmode);
        break;
    case JSR:
        jsr(c);
        break;
    case LDA:
        lda(c, code.addrmode);
        break;
    case LDX:
        ldx(c, code.addrmode);
        break;
    case LDY:
        ldy(c, code.addrmode);
        break;
    case LSR:
        lsr(c, code.addrmode);
        break;
    case NOP:
        nop(c, code.addrmode);
        break;
    case ORA:
        ora(c, code.addrmode);
        break;
    case ROL:
        rol(c, code.addrmode);
        break;
    case ROR:
        ror(c, code.addrmode);
        break;
    case RTI:
        rti(c);
        break;
    case RTS:
        rts(c);
        break;
    case SBC:
        sbc(c, code.addrmode);
        break;
    case STA:
        sta(c, code.addrmode);
        break;
    case STX:
        stx(c, code.addrmode);
        break;
    case STY:
        sty(c, code.addrmode);
        break;

    // unofficial
    case ALR:
        alr(c, code.addrmode);
        break;
    case ANC:
        anc(c, code.addrmode);
        break;
    case ARR:
        arr(c, code.addrmode);
        break;
    case LAX:
        lax(c, code.addrmode);
        break;
    case SAX:
        sax(c, code.addrmode);
        break;
    case DCP:
        dcp(c, code.addrmode);
        break;
    case ISB:
        isb(c, code.addrmode);
        break;
    case SLO:
        slo(c, code.addrmode);
        break;
    case SRE:
        sre(c, code.addrmode);
        break;
    case RLA:
        rla(c, code.addrmode);
        break;
    case RRA:
        rra(c, code.addrmode);
        break;

    default:
        warning_args("%s %x\n", "Undefined instruction execution", hex);
        break;
    }
    return tick(c->bus, code.cycle);
}

static void check_nmi_state(cpu *c)
{
    if (c->bus->ppu->NMI_interupt)
    {
        nmi_interupt(c);
    }
}
