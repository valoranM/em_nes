#include "core/cpu/addr_mode.h"
#include "core/cpu/instruction.h"
#include <stdint.h>

struct opcode
{
    enum Instruction opcode;
    char name[4];
    uint8_t cycle;
    enum AddrMode addrmode;
    union {
        uint8_t nothing;
        enum BranchInstr branch;
        enum RegisterInstr reg;
        enum StackInstr stack;
        enum FlagsInstr flag;
    };
};

extern struct opcode get_opcode(uint8_t hex);
