#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "core/controllers/joypad.h"

joypad *new_joypad(void)
{
    joypad *joy = malloc(sizeof(joypad));

    joy->strobe = false;
    joy->button_index = 0;
    joy->joypad_button = 0;
    return joy;
}

void free_joypad(joypad *joypad)
{
    free(joypad);
}

void write_joypad(joypad *joypad, uint8_t data)
{
    joypad->strobe = (data & 1) == 1;
    if (joypad->strobe)
    {
        joypad->button_index = 0;
    }
}

int read_joypad(joypad *joypad)
{
    if (joypad->button_index > 7)
    {
        return 1;
    }
    uint8_t res = (joypad->joypad_button & (1 << joypad->button_index)) >>
                  joypad->button_index;
    if (!joypad->strobe && joypad->button_index <= 7)
    {
        joypad->button_index++;
    }

    return res;
}

void set_button_pressed_status(joypad *joypad, uint8_t button, bool pressed)
{
    if (pressed)
    {
        joypad->joypad_button |= button;
    }
    else
    {
        joypad->joypad_button &= ~button;
    }
}
