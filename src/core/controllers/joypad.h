#ifndef JOYPAD_C
#define JOYPAD_C

#include <stdbool.h>
#include <stdint.h>

struct joypad_s
{
    bool strobe;
    uint8_t button_index;
    uint8_t joypad_button;
};
typedef struct joypad_s joypad;

#define B_A       0x01
#define B_B       0x02
#define B_SELECT  0x04
#define B_START   0x08
#define B_UP      0x10
#define B_DOWN    0x20
#define B_LEFT    0x40
#define B_RIGHT   0x80

extern joypad *new_joypad(void);
extern void free_joypad(joypad *joypad);
extern void write_joypad(joypad *joypad, uint8_t data);
extern int read_joypad(joypad *joypad);
extern void set_button_pressed_status(joypad *joypad, uint8_t button,
                                      bool pressed);

#endif // !JOYPAD_C
