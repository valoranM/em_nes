#ifndef BUS_H
#define BUS_H

#include <stdint.h>

#include "core/bus/bus_const.h"
#include "core/card/rom.h"
#include "core/controllers/joypad.h"
#include "core/frame/frame.h"
#include "core/ppu/ppu.h"

/*
+---------------+ $10000 +---------------+ $10000
|               |        | PRG-ROM       |
|               |        | Upper Bank    |
| PRG-ROM       |        +---------------+ $C000
|               |        | PRG-ROM       |
|               |        | Lower Bank    |
+---------------+ $8000  +---------------+ $8000
| SRAM          |        | SRAM          |
+---------------+ $6000  +---------------+ $6000
| Expension ROM |        | Expension ROM |
+---------------+ $4020  +---------------+ $4020
|               |        | I/O Registers |
|               |        +---------------+ $4000
|               |        | Mirrors       |
| I/O Registers |        | $2000-$20007  |
|               |        |               |
|               |        +---------------+ $2008
|               |        | I/O Registers |
+---------------+ $2000  +---------------+ $2000
|               |        | Mirrors       |
|               |        | $0000-$07FF   |
|               |        |               |
|               |        +---------------+ $0800
| RAM           |        | Ram           |
|               |        +---------------+ $0200
|               |        | Stack         |
|               |        +---------------+ $0100
|               |        | Zero Page     |
+---------------+ $0000  +---------------+ $0000
*/

struct s_bus
{
    uint8_t ram[RAM_SIZE];
    rom *rom;
    ppu *ppu;
    joypad *j1;
    uint32_t cycle;
};
typedef struct s_bus bus;

/*
 * Initialize bus (set 0 every where)
 * @return bus* new bus
 **/
extern bus *init_bus(rom *rom, ppu *ppu, joypad *j1);

/*
 * free bus
 * @param bus*
 * @return void
 */
extern void free_bus(bus *b);

extern bool tick(bus *bus, uint8_t t);

/*
 * Read 8 bits address in CPU memory map
 * @param uint16_t addr
 * @return uint8_t CPU_MEM[addr]
 **/
extern uint8_t read_mem8(uint16_t addr, bus *mem);

/*
 * Read 16 bits address in CPU memory map
 * @param uint16_t addr
 * @return uint8_t CPU_MEM[addr]
 **/
extern uint16_t read_mem16(uint16_t addr, bus *mem);

extern uint16_t read_mem16_u8addr(uint8_t addr, bus *mem);

/*
 * Write 8 bits value to address in CPU memory
 * @param uint16_t address
 * @param uint8_t value
 **/
extern void write_mem8(uint16_t addr, uint8_t val, bus *mem);

/*
 * Write 16 bits value to address in CPU memory
 * @param uint16_t address
 * @param uint8_t value
 **/
extern void write_mem16(uint16_t addr, uint16_t val, bus *mem);

#endif // BUS_H
