#ifndef BUS_CONST_H
#define BUS_CONST_H

#define RAM_SIZE 0x2000
#define RAM_MIRRORS_START 0x800
// 0x800 - 1 => 1000_0000_0000 -1 => 111_1111_1111
#define RAM_MIRRORS_MASK (RAM_MIRRORS_START - 1)

#define IO_MIRRORED_END 0x4000
#define IO_REGISTERS_END 0x4020

#define PRG_START 0x8000

#define NMI_ADRESS 0xFFFA
#define IRQ_ADRESS 0xFFFE
#define RESET_ADDRESS 0xFFFC

typedef enum 
{
    PPU_Controller = 0x2000,
    PPU_Mask       = 0x2001,
    PPU_Status     = 0x2002,
    OAM_Adress     = 0x2003,
    OAM_Data       = 0x2004,
    PPU_Scroll     = 0x2005,
    PPU_Address    = 0x2006,
    PPU_Data       = 0x2007,

    J1_ADDR        = 0x4016,

    OAM_DMA        = 0x4014,
} IO_regs;

#endif // BUS_CONST_H
