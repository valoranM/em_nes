#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/bus/bus.h"
#include "core/bus/bus_const.h"
#include "core/card/rom.h"
#include "controllers/event.h"
#include "core/controllers/joypad.h"
#include "core/frame/frame.h"
#include "core/frame/render.h"
#include "core/ppu/ppu.h"
#include "utils/debug.h"

static uint16_t read_io_regs(uint16_t addr, bus *mem);
static void write_io_regs(uint16_t addr, uint8_t val, bus *mem);

// data structure

bus *init_bus(rom *rom, ppu *ppu, joypad *j1)
{
    bus *res = malloc(sizeof(bus));
    res->rom = rom;
    res->ppu = ppu;
    res->cycle = 0;
    res->j1 = j1;
    return res;
}

void free_bus(bus *b)
{
    free(b);
}

// emulation

bool tick(bus *bus, uint8_t t)
{
    bus->cycle += t;
    if (ppu_tick(bus->ppu, t * 3))
    {
        if(poll_event(bus->j1))
        {
            return true;
        }
        ppu_render(bus->ppu);
    }
    return false;
}

uint8_t read_mem8(uint16_t addr, bus *mem)
{
    if (addr < RAM_SIZE)
    {
        return mem->ram[addr & RAM_MIRRORS_MASK];
    }

    if (addr < IO_MIRRORED_END)
        addr = 0x2000 + (addr - 0x2000) % 0x8;

    if (addr < IO_REGISTERS_END)
    {
        return read_io_regs(addr, mem);
    }

    if (addr > PRG_START)
    {
        return read_prg_rom(addr, mem->rom);
    }

    warning_args("Ignoring mem read-access at Ox%X\n", addr);
    return 0;
}

static uint16_t read_io_regs(uint16_t addr, bus *mem)
{
    switch (addr)
    {
    case PPU_Status:
        return read_ppu_status(mem->ppu);
    case OAM_Data:
        return read_oam_data(mem->ppu);
    case PPU_Data:
        return read_ppu_data(mem->ppu);
    case J1_ADDR:
        return read_joypad(mem->j1);
    }
    if (addr >= 0x4000 || addr <= 0x415)
    {
        // warning_args("Ignoring mem read-access at Ox%X\n", addr);
        return 0; // APU
    }
    warning_args("Ignoring mem read-access at Ox%X\n", addr);
    return 0;
}

void write_mem8(uint16_t addr, uint8_t val, bus *mem)
{
    if (addr < RAM_SIZE)
    {
        mem->ram[addr & RAM_MIRRORS_MASK] = val;
        return;
    }

    if (addr < IO_MIRRORED_END)
        addr = 0x2000 + (addr - 0x2000) % 0x8;

    if (addr < IO_REGISTERS_END)
    {
        write_io_regs(addr, val, mem);
        return;
    }

    if (addr >= PRG_START)
    {
        write_prg_rom(addr);
        return;
    }
    // warning_args("Ignoring mem write-access at Ox%X\n", addr);
}

static void write_io_regs(uint16_t addr, uint8_t val, bus *mem)
{
    switch (addr)
    {
    case PPU_Controller:
        write_to_ppu_ctrl(mem->ppu, val);
        break;
    case PPU_Mask:
        write_to_ppu_mask(mem->ppu, val);
        break;
    case OAM_Adress:
        write_to_oam_addr(mem->ppu, val);
        break;
    case OAM_Data:
        write_to_oam_data(mem->ppu, val);
        break;
    case PPU_Scroll:
        write_to_ppu_scroll(mem->ppu, val);
        break;
    case PPU_Address:
        write_to_ppu_addr(mem->ppu, val);
        break;
    case PPU_Data:
        write_to_ppu_data(mem->ppu, val);
        break;
    case OAM_DMA: {
        uint8_t data[256];
        uint16_t hi = val << 8;
        for (uint16_t i = 0; i < 256; i++)
        {
            data[i] = read_mem8(hi + i, mem);
        }
        write_to_oam_dma(mem->ppu, data);
    }
    break;

    case J1_ADDR:
        write_joypad(mem->j1, val);
        break;
    default:
        if (addr >= 0x4000 || addr <= 0x415)
        {
            return; // APU
        }
        break;
    }
}

/*
    $addr = aabb

      +---+
     /|aa | addr + 1
     \|bb | addr
      +---+
*/
uint16_t read_mem16(uint16_t addr, bus *mem)
{
    uint8_t lo = read_mem8(addr, mem);
    uint8_t hi = read_mem8(addr + 1, mem);
    return ((uint16_t)hi << 8) + lo;
}

uint16_t read_mem16_u8addr(uint8_t addr, bus *mem)
{
    uint8_t lo = read_mem8(addr, mem);
    uint8_t hi = read_mem8((addr + 1) & 0xff, mem);
    return ((uint16_t)hi << 8) + lo;
}

/*
    $addr <- aabb

    aa = val >> 8
    bb = val & 0xff

      +---+
     /|aa | addr + 1
     \|bb | addr
      +---+
*/
void write_mem16(uint16_t addr, uint16_t val, bus *mem)
{
    uint8_t lo = val & 0xff;
    write_mem8(addr, lo, mem);
    uint8_t hi = val >> 8;
    write_mem8(addr + 1, hi, mem);
}
