#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "core/frame/frame.h"
#include "core/frame/pallete.h"
#include "core/frame/render.h"
#include "core/ppu/ppu.h"
#include "core/ppu/register.h"
#include "views/draw.h"

static void draw_bg(ppu *ppu);
static void draw_sprite(ppu *ppu, bool fg);

static double delay = 1000.0 / 90.0;
static uint32_t frame_start;

void ppu_render(ppu *ppu)
{
    draw_sprite(ppu, false);
    draw_bg(ppu);
    draw_sprite(ppu, true);

    uint32_t frame_time = SDL_GetTicks() - frame_start;
    if (frame_time < delay)
    {
        SDL_Delay(delay - frame_time);
    }
    draw_frame(ppu->frame);
    frame_start = SDL_GetTicks();
}

static void draw_tile_bg(ppu *ppu, uint8_t tile, uint8_t rgb[3], uint8_t col,
                         uint8_t row, uint16_t bank);
static void bg_pallette(ppu *ppu, uint8_t rgb[3], uint16_t til_col,
                        uint16_t row_col);

static void draw_bg(ppu *ppu)
{
    uint16_t bank = bknd_pattern_addr(ppu);

    for (uint16_t i = 0; i < 0x3C0; i++)
    {
        uint8_t tile = ppu->vram[i];
        uint8_t tile_column = i % 32;
        uint8_t tile_row = i / 32;
        uint8_t rgb_pal[3];
        bg_pallette(ppu, rgb_pal, tile_column, tile_row);

        draw_tile_bg(ppu, tile, rgb_pal, tile_column * 8, tile_row * 8, bank);
    }
}

static uint8_t *get_rgb(ppu *ppu, uint8_t pallette[3], uint8_t val);

static void draw_tile_bg(ppu *ppu, uint8_t tile, uint8_t rgb_pal[3],
                         uint8_t col, uint8_t row, uint16_t bank)
{
    uint32_t start = bank + tile * 16;

    for (int y = 0; y < 8; y++)
    {
        uint8_t upper = ppu->chr_rom[start + y];
        uint8_t lower = ppu->chr_rom[start + y + 8];
        for (int x = 7; x >= 0; x--)
        {
            uint8_t val = (1 & lower) << 1 | (1 & upper);
            upper = upper >> 1;
            lower = lower >> 1;

            uint8_t *rgb = get_rgb(ppu, rgb_pal, val);
            set_pixel(ppu->frame, col + x, row + y, rgb[0], rgb[1], rgb[2]);
        }
    }
}

static void bg_pallette(ppu *ppu, uint8_t rgb[3], uint16_t tile_col,
                        uint16_t tile_row)
{
    uint16_t table_idx = (tile_row / 4) * 8 + tile_col / 4;
    uint8_t attr = ppu->vram[0x3c0 + table_idx];

    attr = attr >> (((tile_col % 4 / 2) + (tile_row % 4 / 2) * 2) * 2);
    attr &= 0x03;

    uint16_t start = 1 + attr * 4;

    rgb[0] = ppu->palettes[start];
    rgb[1] = ppu->palettes[start + 1];
    rgb[2] = ppu->palettes[start + 2];
}

static void draw_tile_sprt(ppu *ppu, uint8_t tile, uint8_t rgb_pal[3],
                           bool flip_v, bool flip_h, uint8_t tile_x,
                           uint8_t tile_y, uint16_t bank);
static void sprt_pallette(ppu *ppu, uint8_t pal[3], uint8_t pallette_idx);

static void draw_sprite(ppu *ppu, bool fg)
{
    uint16_t bank = sprt_pattern_addr(ppu);
    for (int i = 0x100 - 4; i >= 0; i -= 4)
    {
        bool sprit_fg = (ppu->oam_data[i + 2] & 0x20) == 0;
        if ((fg && !sprit_fg) || (!fg && sprit_fg))
        {
            continue;
        }

        uint8_t tile_id = ppu->oam_data[i + 1];
        uint8_t tile_x = ppu->oam_data[i + 3];
        uint8_t tile_y = ppu->oam_data[i];

        bool flip_v = (ppu->oam_data[i + 2] & 0x80) != 0;
        bool flip_h = (ppu->oam_data[i + 2] & 0x40) != 0;
        uint8_t pallette_idx = (ppu->oam_data[i + 2] & 0x3);

        uint8_t rgb_pal[3];
        sprt_pallette(ppu, rgb_pal, pallette_idx);
        draw_tile_sprt(ppu, tile_id, rgb_pal, flip_v, flip_h, tile_x, tile_y,
                       bank);
    }
}

static void draw_tile_sprt(ppu *ppu, uint8_t tile, uint8_t rgb_pal[3],
                           bool flip_v, bool flip_h, uint8_t tile_x,
                           uint8_t tile_y, uint16_t bank)
{
    uint32_t start = bank + tile * 16;
    for (int y = 0; y < 8; y++)
    {
        uint8_t upper = ppu->chr_rom[start + y];
        uint8_t lower = ppu->chr_rom[start + y + 8];
        for (int x = 7; x >= 0; x--)
        {
            uint8_t val = (1 & lower) << 1 | (1 & upper);
            upper = upper >> 1;
            lower = lower >> 1;

            if (val == 0)
            {
                continue;
            }
            uint8_t *rgb = get_rgb(ppu, rgb_pal, val);

            uint16_t x_pos = tile_x + x;
            uint16_t y_pos = tile_y + y;
            if (flip_h)
            {
                x_pos = tile_x + 7 - x;
            }
            if (flip_v)
            {
                y_pos = tile_y + 7 - y;
            }
            set_pixel(ppu->frame, x_pos, y_pos, rgb[0], rgb[1], rgb[2]);
        }
    }
}

static void sprt_pallette(ppu *ppu, uint8_t pal[3], uint8_t pallette_idx)
{
    uint8_t start = 0x11 + (pallette_idx * 4);
    pal[0] = ppu->palettes[start];
    pal[1] = ppu->palettes[start + 1];
    pal[2] = ppu->palettes[start + 2];
}

static uint8_t *get_rgb(ppu *ppu, uint8_t pallette[3], uint8_t val)
{
    switch (val)
    {
    case 0:
        return system_pallete[ppu->palettes[0]];
    case 1:
        return system_pallete[pallette[0]];
    case 2:
        return system_pallete[pallette[1]];
    case 3:
        return system_pallete[pallette[2]];
    }
    return NULL;
}
