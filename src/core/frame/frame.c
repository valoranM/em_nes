#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "core/frame/frame.h"
// #include "frame/pallete.h"

frame *new_frame(void)
{
    frame *res = (frame *)malloc(sizeof(frame));
    // res->pallete = (uint8_t **)system_pallete;
    return res;
}

void free_frame(frame *f)
{
    free(f);
}

void set_pixel(frame *f, uint16_t x, uint16_t y, uint8_t r, uint8_t g,
               uint8_t b)
{
    uint32_t base = y * 3 * WIDTH + x * 3;
    if (base + 2 < FRAM_LEN)
    {
        f->data[base] = r;
        f->data[base + 1] = g;
        f->data[base + 2] = b;
    }
}

uint8_t *get_pixel(frame *f, uint16_t x, uint16_t y)
{
    return &(f->data[x * 3 + y * 3 * WIDTH]);
}
