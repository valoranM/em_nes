#ifndef FRAME_H
#define FRAME_H

#include <stdint.h>

#define WIDTH 256
#define HEIGHT 240
#define FRAM_LEN (WIDTH * HEIGHT * 3)

struct frame_s
{
    uint8_t data[FRAM_LEN];
    uint8_t **pallete;
};

typedef struct frame_s frame;

extern frame *new_frame(void);
extern void free_frame(frame *f);
extern void set_pixel(frame *f, uint16_t x, uint16_t y, uint8_t r, uint8_t g,
                      uint8_t b);
extern uint8_t *get_pixel(frame *f, uint16_t x, uint16_t y);

#endif // !FRAME_H
