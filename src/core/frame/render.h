#ifndef RENDER_H
#define RENDER_H

#include "core/frame/frame.h"
#include "core/ppu/ppu.h"

extern void ppu_render(ppu *ppu);

#endif // !RENDER_H
