#ifndef NES_H
#define NES_H

#include "core/bus/bus.h"
#include "core/card/rom.h"
#include "core/controllers/joypad.h"
#include "core/cpu/cpu.h"
#include "core/ppu/ppu.h"
#include "frame/frame.h"

struct nes_s
{
    cpu *cpu;
    bus *bus;
    ppu *ppu;
    rom *rom;
    frame *frame;
    joypad *j1;
};
typedef struct nes_s nes;

extern nes *init_nes(rom *rom);
extern void free_nes(nes *nes);

#endif // !NES_H
