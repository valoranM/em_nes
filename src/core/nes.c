#include <stdlib.h>

#include "core/card/rom.h"
#include "core/nes.h"

nes *init_nes(rom *rom)
{
    frame *frame = new_frame();
    ppu *ppu = init_ppu(rom, frame);
    joypad *j1 = new_joypad();
    bus *bus = init_bus(rom, ppu, j1);
    cpu *cpu = init_cpu(bus);

#if TEST != 1
    reset_cpu(cpu);
#else
    tick(bus, 7);
#endif // !TEST == 1

    nes *res = (nes *)malloc(sizeof(nes));
    res->rom = rom;
    res->frame = frame;
    res->ppu = ppu;
    res->bus = bus;
    res->cpu = cpu;
    res->j1 = j1;

    return res;
}

void free_nes(nes *nes)
{
    free_cpu(nes->cpu);
    free_ppu(nes->ppu);
    free_bus(nes->bus);
    free_rom(nes->rom);
    free_frame(nes->frame);
    free_joypad(nes->j1);
    free(nes);
}
