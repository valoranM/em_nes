#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/card/rom.h"
#include "utils/debug.h"

static void check_format(uint8_t *header, rom *rom);
static void set_mirror(uint8_t *header, rom *rom);
static void read_prg(rom *rom);
static void read_chr(rom *rom);

static FILE *rom_file;

void read_rom(rom *rom, const char *file)
{
    rom_file = fopen(file, "rb");
    if (rom_file == NULL)
    {
        free_rom(rom);
        error_args("file '%s' not found\n", file);
    }
    LOG("ROM file = '%s'\n", file);

    uint8_t header[INES_HEADER_SIZE];
    fread(header, INES_HEADER_SIZE, 1, rom_file);

    check_format(header, rom);
    set_mirror(header, rom);
    rom->trainner = (header[6] & 0x04) != 0;
    if (rom->trainner)
    {
        fclose(rom_file);
        free_rom(rom);
        error("trainner is not supported");
    }
    rom->trainner = (header[6] & 0x02) != 0;
    if (rom->trainner)
    {
        fclose(rom_file);
        free_rom(rom);
        error("battery-backed RAM is not supported");
    }
    rom->version = (header[7] >> 2) & 0x3;
    if (rom->version == INES_V2)
    {
        fclose(rom_file);
        free_rom(rom);
        error("NES2.0 format is not supported");
    }
    rom->mapper = (header[7] & 0xF0) & (header[6] >> 4);
    rom->PRG_banks = header[4];
    rom->CHR_banks = header[5];

    read_prg(rom);
    read_chr(rom);

    PRINT_ROM(rom);

    fclose(rom_file);
}

static void check_format(uint8_t *header, rom *rom)
{
    if (strncmp((char *)header, "NES\x1A", 4) != 0)
    {
        fclose(rom_file);
        free_rom(rom);
        error("unknown file format");
    }
}

static void set_mirror(uint8_t *header, rom *rom)
{
    if ((header[6] & 0x08) != 0)
    {
        rom->mirror = FOUR_SCREEN;
    }
    else if ((header[6] & 0x01) != 0)
    {
        rom->mirror = VERTICAL;
    }
    else
    {
        rom->mirror = HORIZONTAL;
    }
}

static void read_prg(rom *rom)
{
    uint32_t prg_size = rom->PRG_banks * PRG_ROM_BANK_SIZE;
    LOG("PRG size = 0x%x\n", prg_size);
    rom->PRG_rom = malloc(prg_size);
    fread(rom->PRG_rom, 1, prg_size, rom_file);
}

static void read_chr(rom *rom)
{
    uint32_t chr_size = rom->CHR_banks * CHR_ROM_BANK_SIZE;
    LOG("CHR size = 0x%x\n", chr_size);
    rom->CHR_rom = malloc(chr_size);
    fread(rom->CHR_rom, 1, chr_size, rom_file);
}
