#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "core/frame/frame.h"

static uint8_t *get_rgb(frame *frame, uint8_t val);

void show_tile(frame *frame, uint8_t *chr_rom, uint32_t bank, uint32_t tile,
               uint32_t x, uint32_t y)
{
    bank = bank * 0x1000;

    uint32_t start = bank + tile * 16;

    for (int y_t = 0; y_t < 8; y_t++)
    {
        uint8_t upper = chr_rom[start + y_t];
        uint8_t lower = chr_rom[start + y_t + 8];
        for (int x_t = 7; x_t >= 0; x_t--)
        {
            uint8_t val = (1 & upper) << 1 | (1 & lower);
            upper = upper >> 1;
            lower = lower >> 1;
            uint8_t *rgb = get_rgb(frame, val);
            set_pixel(frame, x + x_t, y + y_t, rgb[0], rgb[1], rgb[2]);
        }
    }
}

extern void show_bank(frame *frame, uint8_t *chr_rom, uint32_t bank)
{
    uint32_t x = 2, y = 2;
    for (int i = 0; i < 255; i++)
    {
        if (i != 0 && i % 20 == 0)
        {
            y += 10;
            x = 2;
        }
        show_tile(frame, chr_rom, bank, i, x, y);
        x += 10;
    }
}

static uint8_t *get_rgb(frame *frame, uint8_t val)
{
    switch (val)
    {
    case 0:
        return ((uint8_t(*)[3])frame->pallete)[0x01];
    case 1:
        return ((uint8_t(*)[3])frame->pallete)[0x23];
    case 2:
        return ((uint8_t(*)[3])frame->pallete)[0x27];
    case 3:
        return ((uint8_t(*)[3])frame->pallete)[0x30];
    }
    return NULL;
}
