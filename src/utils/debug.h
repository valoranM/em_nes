#ifndef ERROR_H
#define ERROR_H

#include <stdint.h>

#include "core/card/rom.h"
#include "core/cpu/cpu.h"

#ifdef PRINT_LOG
#define LOG(...) fprintf(stderr, __VA_ARGS__);

extern void print_rom(rom *rom);
extern void print_cpu_state(cpu *c);

#define PRINT_ROM(rom) print_rom(rom);
#define PRINT_CPU_STATE(cpu) print_cpu_state(cpu);
#else
#define LOG(fmt, ...)
#define PRINT_ROM(rom)
#define PRINT_CPU_STATE(cpu)
#endif // LOG

/*
 * displays the error message and exits the program
 * @param const char* output message
 * @return no return
 */
extern void error(const char *output);

extern void error_args(const char *fmt, ...);

/*
 * displays the warning message
 * @param const char* output message
 * @return
 */
extern void warning(const char *output);

/*
 * displays a special warning messag for instruction execution
 * @param const char* output message
 * @param uint8_t opcode
 */
extern void warning_args(const char *fmt, ...);

#endif // ERROR_H
