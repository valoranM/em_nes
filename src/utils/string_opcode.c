#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "core/bus/bus.h"
#include "core/cpu/addr_mode.h"
#include "core/cpu/cpu.h"
#include "core/cpu/instruction.h"
#include "core/cpu/opcode.h"

#ifdef PRINT_LOG

static void print_hex(uint16_t pc, struct opcode code, cpu *c);
static uint8_t get_nb_args(struct opcode code);
static void print_args(struct opcode code, cpu *cpu, uint16_t pc);
static bool is_official(uint8_t hex, struct opcode code);

void debug_opcode(cpu *c)
{
    uint16_t pc = c->pc;
    uint8_t hex = read_mem8(pc, c->bus);
    struct opcode code = get_opcode(hex);
    print_hex(pc, code, c);
    pc++;

    fprintf(stderr, "%s%s", (!(is_official(hex, code)) ? "*" : " "), code.name);
    print_args(code, c, pc);
    fprintf(stderr, "A:%02X X:%02X Y:%02X P:%02X SP:%02X PPU:%3d,%3d CYC:%d", //
            c->a, c->x, c->y, c->p, c->sp, c->bus->ppu->scanline,
            c->bus->ppu->cycle, c->bus->cycle);
    fprintf(stderr, "\n");
}

static void print_hex(uint16_t pc, struct opcode code, cpu *c)
{
    uint8_t hex = read_mem8(pc, c->bus);
    fprintf(stderr, "%02X ", hex);

    uint8_t nb_param = get_nb_args(code);
    for (uint8_t i = 0; i < 2; i++)
    {
        if (i < nb_param)
        {
            fprintf(stderr, "%02X ", read_mem8(pc + i + 1, c->bus));
        }
        else
        {
            fprintf(stderr, "   ");
        }
    }
}

static uint8_t get_nb_args(struct opcode code)
{
    switch (code.addrmode)
    {
    case Implied:
    case Accumaltor:
    case None:
        return 0;
    case Immediate:
    case Zero_Page:
    case Zero_Page_X:
    case Zero_Page_Y:
    case Relative:
    case Indirect_Indexed:
    case Indexed_Indirect:
        return 1;
    default:
        return 2;
    }
}

static void print_args(struct opcode code, cpu *cpu, uint16_t pc)
{
    uint8_t arg8, hi, lo;
    uint16_t arg16;
    uint16_t stored_value;
    char output[30] = "";
    switch (code.addrmode)
    {
    case Accumaltor:
        sprintf(output, "A");
        break;
    case Immediate:
        arg8 = read_mem8(pc, cpu->bus);
        sprintf(output, "#$%02X", arg8);
        break;
    case Absolute:
        arg16 = read_mem16(pc, cpu->bus);
        // if (code.opcode == JMP || code.opcode == JSR)
        {
            sprintf(output, "$%04X", arg16);
        }
        // else
        // {
        //     sprintf(output, "$%04X = %02X", arg16, read_mem8(arg16,
        //     cpu->bus));
        // }
        break;
    case Zero_Page:
        arg8 = read_mem8(pc, cpu->bus);
        stored_value = read_mem8(arg8, cpu->bus);
        sprintf(output, "$%02X = %02X", arg8, stored_value);
        break;
    case Zero_Page_X:
        arg8 = read_mem8(pc, cpu->bus);
        stored_value = read_mem8((uint8_t)(arg8 + cpu->x), cpu->bus);
        sprintf(output, "$%02X,X @ %02X = %02X", arg8, (uint8_t)(arg8 + cpu->x),
                stored_value);
        break;
    case Zero_Page_Y:
        arg8 = read_mem8(pc, cpu->bus);
        stored_value = read_mem8((uint8_t)(arg8 + cpu->y), cpu->bus);
        sprintf(output, "$%02X,Y @ %02X = %02X", arg8, (uint8_t)(arg8 + cpu->y),
                stored_value);
        break;
    case Ab_Indexed_X:
        arg16 = read_mem16(pc, cpu->bus);
        stored_value = read_mem8(arg16 + cpu->x, cpu->bus);
        sprintf(output, "$%04X,X @ %04X = %02X", arg16,
                (uint16_t)(arg16 + cpu->x), stored_value);
        break;
    case Ab_Indexed_Y:
        arg16 = read_mem16(pc, cpu->bus);
        stored_value = read_mem8(arg16 + cpu->y, cpu->bus);
        sprintf(output, "$%04X,Y @ %04X = %02X", arg16,
                (uint16_t)(arg16 + cpu->y), stored_value);
        break;
    case Indirect_Abs:
        arg16 = read_mem16(pc, cpu->bus);
        lo = read_mem8(arg16, cpu->bus);
        hi = read_mem8((arg16 & 0xFF00) | ((arg16 + 1) & 0xFF), cpu->bus);
        sprintf(output, "($%04X) = %04X", arg16, (hi << 8) | lo);
        break;
    case Indexed_Indirect:
        arg8 = read_mem8(pc, cpu->bus);
        stored_value = read_mem16_u8addr(arg8 + cpu->x, cpu->bus);
        sprintf(output, "($%02X,X) @ %02X = %04X = %02X", arg8,
                (uint8_t)(arg8 + cpu->x), stored_value,
                read_mem8(stored_value, cpu->bus));
        break;
    case Indirect_Indexed:
        arg8 = read_mem8(pc, cpu->bus);
        stored_value = read_mem16_u8addr(arg8, cpu->bus);
        sprintf(output, "($%02X),Y = %04X @ %04X = %02X", arg8, stored_value,
                (uint16_t)(stored_value + cpu->y),
                read_mem8(stored_value + cpu->y, cpu->bus));
        break;
    case Relative:
        arg8 = read_mem8(pc, cpu->bus);
        sprintf(output, "$%04X", (uint16_t)((int8_t)arg8 + pc + 1));
    default:
        break;
    }
    fprintf(stderr, " %-28s", output);
}

static bool is_official(uint8_t hex, struct opcode code)
{
    switch (code.opcode)
    {
    case ALR:
    case ANC:
    case ARR:
    case AXS:
    case LAX:
    case SAX:
    case DCP:
    case ISB:
    case RLA:
    case RRA:
    case SLO:
    case SRE:
    case SKB:
    case IGN:
        return false;
    case SBC:
        return hex != 0xeb;
    case NOP:
        return code.addrmode == None;
    default:
        return true;
    }
}

#endif // PRINT_LOG
