#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "core/card/rom.h"
#include "core/cpu/cpu.h"
#include "utils/debug.h"

#ifdef PRINT_LOG

void print_rom(rom *rom)
{
    fprintf(stderr, "----------- ROM -----------\n");
    fprintf(stderr, " PRG banks : \t%d\n", rom->PRG_banks);
    fprintf(stderr, " CHR banks : \t%d\n", rom->CHR_banks);
    fprintf(stderr, " Mappers : \t%d\n", rom->mapper);
    switch (rom->mirror)
    {
    case HORIZONTAL:
        fprintf(stderr, " Mirror : \tHorizontal\n");
        break;
    case VERTICAL:
        fprintf(stderr, " Mirror : \tVertical\n");
        break;
    case FOUR_SCREEN:
        fprintf(stderr, " Mirror : \tFour screen\n");
        break;
    }
    switch (rom->version)
    {
    case INES_V1:
        fprintf(stderr, " Version : \tiNES1.0\n");
        break;
    case INES_V2:
        fprintf(stderr, " Version : \tiNES2.0\n");
        break;
    }
    fprintf(stderr, " Trainner : \t%c\n", (rom->trainner) ? 'T' : 'F');
    fprintf(stderr, " Battery RAM : \t%c\n", (rom->battery_ram) ? 'T' : 'F');
    fprintf(stderr, "---------------------------\n");
}

// in string_opcode.c
extern void debug_opcode(cpu *c);

void print_cpu_state(cpu *c)
{
    fprintf(stderr, "%04X  ", c->pc);

    debug_opcode(c);
}

#endif // PRINT_LOG

__attribute__((noreturn)) void error(const char *output)
{
    fprintf(stderr, "[ERROR] : %s\n", output);
    exit(EXIT_FAILURE);
}

__attribute__((noreturn)) void error_args(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "[ERROR] : ");
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(EXIT_FAILURE);
}

void warning(const char *output)
{
    fprintf(stderr, "[WARNING] : %s\n", output);
}

void warning_args(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "[WARNING] : ");
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}
