#ifndef TILES_VIEW
#define TILES_VIEW

#include <stdint.h>

#include "core/frame/frame.h"

extern void show_tile(frame *frame, uint8_t *chr_rom, uint32_t bank,
                      uint32_t tile, uint32_t x, uint32_t y);
extern void show_bank(frame *frame, uint8_t *chr_rom, uint32_t bank);

#endif // !TILES_VIEW
